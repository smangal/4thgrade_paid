package com.mathfriendzy.controller.friendzy;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.forthgradepaid.R;
import com.mathfriendzy.controller.friendzy.calender.CalenderView;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.model.friendzy.FriendzyDTO;
import com.mathfriendzy.model.friendzy.FriendzyServerOperation;
import com.mathfriendzy.model.grade.Grade;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;

public class CreateChallengeActivity extends AdBaseActivity implements OnClickListener
{
	private TextView	mfTitleHomeScreen				= null;
	private TextView	txtClassname					= null;
	private TextView	txtGrade						= null;
	private TextView	txtSchool						= null;
	private TextView	txtGradewide					= null;
	private TextView	txtStudent						= null;
	private TextView	txtStartdate					= null;
	private TextView	txtEnddate						= null;

	private Button btnSchoolInfo						= null;
	private Button btnStudentInfo						= null;
	private Button btnGradeInfo							= null;
	private Button chkSchool							= null;
	private Button chkGrade								= null;
	private Button chkStudent							= null;
	private Button btnSave								= null;
	private Button btnAddRewards						= null;
	private Button btnDelete							= null;

	private ImageButton	imgInfo							= null;
	private ArrayList<String> gradeList			        = null;

	private RelativeLayout layoutSpin					= null;
	private Spinner spinGrade							= null;

	private TextView edtStartdate						= null;
	private TextView edtEnddate							= null;
	private EditText edtClassname						= null;

	private String infoImg								= null;
	private String infoschool							= null;
	private String infograde							= null;
	private String infostud								= null;
	private String startAlert							= null;
	private String endAlert								= null;
	private String classAlert							= null;
	private String challengerId							= "0";

	int START_DATE										= 2;
	int END_DATE										= 3;

	public static FriendzyDTO friendzyObj				= null;
	private String status								= "1";
	private String type									= "3";
	private String currentDateandTime					= null;

	private boolean isStartDateValid					= true;
	private boolean isEndDateValid						= true;
	private int grey_radio_btn_id;
	private int green_radio_btn_id;

	private String activeMsg							= null;


	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_challenges);

		challengerId	= getIntent().getStringExtra("challengerId");
		UserPlayerOperation user = new UserPlayerOperation(this);
		friendzyObj = user.getTeacherDetails();
		getWidgetId();
		setWidgetText();

		if(MainActivity.isTab)
		{
			green_radio_btn_id 	= R.drawable.green_radio_button_ipad;
			grey_radio_btn_id	= R.drawable.grey_radio_button_ipad;
		}
		else
		{
			green_radio_btn_id 	= R.drawable.green_radio_button;
			grey_radio_btn_id	= R.drawable.grey_radio_button;
		}

		if(!challengerId.equals("0"))
		{
			friendzyObj = StudentChallengeActivity.friendzyObj;
			setTextForChallenger();
		}

	}//END onCreate Method


	private void setTextForChallenger()
	{
		edtClassname.setText(friendzyObj.getClassName());
		edtEnddate.setText(friendzyObj.getEndDate());
		edtStartdate.setText(friendzyObj.getStartDate());
		//spinGrade.setSelection(Integer.parseInt(friendzyObj.getGrade()));
		status = friendzyObj.getStatus();
		type   = friendzyObj.getType();
				
		getGrade(this, friendzyObj.getGrade());
		if(status.equals("1"))
			spinGrade.setEnabled(false);
		
		if(friendzyObj.getType().equals("1"))
		{			
			chkGrade.setBackgroundResource(grey_radio_btn_id);
			chkSchool.setBackgroundResource(green_radio_btn_id);
			chkStudent.setBackgroundResource(grey_radio_btn_id);
		}
		else if(friendzyObj.getType().equals("2"))
		{
			chkGrade.setBackgroundResource(green_radio_btn_id);
			chkSchool.setBackgroundResource(grey_radio_btn_id);
			chkStudent.setBackgroundResource(grey_radio_btn_id);
		}
		else if(friendzyObj.getType().equals("3"))
		{
			chkGrade.setBackgroundResource(grey_radio_btn_id);
			chkSchool.setBackgroundResource(grey_radio_btn_id);
			chkStudent.setBackgroundResource(green_radio_btn_id);
		}
		checkStatus(edtStartdate.getText().toString(), edtEnddate.getText().toString());

		if(status.equals("2") && !isEndDateValid)
		{
			btnDelete.setVisibility(View.VISIBLE);
			btnDelete.setOnClickListener(new OnClickListener() {

				@Override
				public void onClick(View v)
				{
					generateDeleteFriendzyChallengeDialog();		
				}
			});
		}

	}


	private void getWidgetId()
	{
		mfTitleHomeScreen	= (TextView) findViewById(R.id.mfTitleHomeScreen);
		txtClassname		= (TextView) findViewById(R.id.txtClassname);
		txtEnddate			= (TextView) findViewById(R.id.txtEndDate);
		txtGrade			= (TextView) findViewById(R.id.txtGrade);
		txtGradewide		= (TextView) findViewById(R.id.txtGradewide);
		txtSchool			= (TextView) findViewById(R.id.txtSchool);
		txtStartdate		= (TextView) findViewById(R.id.txtStartDate);
		txtStudent			= (TextView) findViewById(R.id.txtStudent);

		btnDelete			= (Button) findViewById(R.id.btnDelete);
		btnAddRewards		= (Button) findViewById(R.id.btnAddReward);
		btnGradeInfo		= (Button) findViewById(R.id.btnGradeInfo);
		btnSave				= (Button) findViewById(R.id.btnSave);
		btnSchoolInfo		= (Button) findViewById(R.id.btnSchoolInfo);
		btnStudentInfo		= (Button) findViewById(R.id.btnStudentInfo);
		edtStartdate		= (TextView) findViewById(R.id.edtStartDate);
		edtEnddate			= (TextView) findViewById(R.id.edtEndDate);
		edtClassname		= (EditText) findViewById(R.id.edtClassName);

		chkGrade			= (Button) findViewById(R.id.chkGrade);
		chkStudent			= (Button) findViewById(R.id.chkStudent);
		chkSchool			= (Button) findViewById(R.id.chkSchool);

		spinGrade			= (Spinner) findViewById(R.id.spinGrade);
		layoutSpin			= (RelativeLayout) findViewById(R.id.layoutSpin);
		imgInfo				= (ImageButton) findViewById(R.id.imgInfo);

		btnAddRewards.setOnClickListener(this);		
		btnGradeInfo.setOnClickListener(this);
		btnSchoolInfo.setOnClickListener(this);
		btnStudentInfo.setOnClickListener(this);
		btnSave.setOnClickListener(this);
		layoutSpin.setOnClickListener(this);
		edtStartdate.setOnClickListener(this);
		edtEnddate.setOnClickListener(this);
		chkGrade.setOnClickListener(this);
		chkStudent.setOnClickListener(this);
		chkSchool.setOnClickListener(this);
		imgInfo.setOnClickListener(this);
		//spinGrade.setOnClickListener(this);

	}//End getWidgetId method



	private void setWidgetText()
	{
		getGrade(this, "");
		Translation tr = new Translation(this);
		tr.openConnection();

		mfTitleHomeScreen.setText(tr.getTranselationTextByTextIdentifier("btnTitleCreateChallenge")+"");
		txtClassname.setText(tr.getTranselationTextByTextIdentifier("lblClass")+" " +
				tr.getTranselationTextByTextIdentifier("lblRegName")+":");

		edtClassname.setHint(tr.getTranselationTextByTextIdentifier("lblClass")+" " +
				tr.getTranselationTextByTextIdentifier("lblRegName"));

		txtGrade.setText(tr.getTranselationTextByTextIdentifier("lblAddPlayerGrade")+":");
		txtGradewide.setText(tr.getTranselationTextByTextIdentifier("lblAddPlayerGrade")+" "+
				tr.getTranselationTextByTextIdentifier("lblWide"));
		txtSchool.setText(tr.getTranselationTextByTextIdentifier("lblRegSchool")+" "+
				tr.getTranselationTextByTextIdentifier("lblWide"));

		txtEnddate.setText(tr.getTranselationTextByTextIdentifier("lblEnd")+
				tr.getTranselationTextByTextIdentifier("mfLblDate")+":");
		txtStartdate.setText(tr.getTranselationTextByTextIdentifier("lblStart")+
				tr.getTranselationTextByTextIdentifier("mfLblDate")+":");

		txtStudent.setText(tr.getTranselationTextByTextIdentifier("resultTitleMyStudents")+"");

		btnAddRewards.setText(tr.getTranselationTextByTextIdentifier("lblAddEditRewards")+"");
		btnSave.setText(tr.getTranselationTextByTextIdentifier("btnTitleSave")+"");

		infoImg = tr.getTranselationTextByTextIdentifier("lblPleaseEnterYourClassNameOrUniqueNameForYourChallenge");
		infograde = tr.getTranselationTextByTextIdentifier("alertMsgGradeWidePopUp");
		infoschool	= tr.getTranselationTextByTextIdentifier("alertMsgSchoolWidePopUp");
		infostud = tr.getTranselationTextByTextIdentifier("alertMsgStudentsWidePopUp");
		startAlert = tr.getTranselationTextByTextIdentifier("alertMsgPleaseSelectValidDate");
		endAlert = tr.getTranselationTextByTextIdentifier("alertMsgPleaseSelectEndDate");
		classAlert = tr.getTranselationTextByTextIdentifier("alertMsgPleaseEnterYourClassName");

		activeMsg  = tr.getTranselationTextByTextIdentifier("lblThisOptionCannotChanged");

		//lblEnterRewardsThatYouWouldLikeToOffer
		//lblExampleRewardsOffer
		tr.closeConnection();

		setDate();

	}//END setWidgetText method


	@SuppressWarnings("deprecation")
	private void setDate()
	{
		String space1 = "";
		String space2 = "";

		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		currentDateandTime = sdf.format(new Date());
		edtStartdate.setText(""+currentDateandTime);

		Calendar cal = Calendar.getInstance(Locale.getDefault());			
		cal.add(Calendar.DATE, 7);
		Date date = cal.getTime();
		if(date.getMonth() < 9 )
		{
			space1 = "0";
		}
		if(date.getDate() < 10)
		{
			space2 = "0";
		}
		edtEnddate.setText(space1+(date.getMonth()+1)+"/"+space2+date.getDate()+"/"+(date.getYear()+1900));
	}

	/** 
	 * @Descritpion getGradeData from database 
	 * @param context
	 * @param gradeValue
	 */
	protected void getGrade(Context context,String gradeValue)
	{			
		Grade gradeObj = new Grade();
		gradeList = gradeObj.getGradeList(this);
		this.setGradeAdapter(context,gradeValue);
	}

	/**
	 * @Description set Grade data to adapter
	 * @param context
	 * @param gradeValue
	 */
	protected void setGradeAdapter(Context context,String gradeValue)
	{				
		ArrayAdapter<String> gradeAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1,gradeList);
		gradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinGrade.setAdapter(gradeAdapter);
		spinGrade.setSelection(gradeAdapter.getPosition(gradeValue));

		spinGrade.setOnItemSelectedListener(new OnItemSelectedListener() 
		{
			@Override
			public void onItemSelected(AdapterView<?> parent, View view,
					int pos, long id) {
				((TextView) parent.getChildAt(0)).setTextColor(Color.BLACK);	         

			}
			@Override
			public void onNothingSelected(AdapterView<?> arg0) {

			}
		});
	}

	@Override
	public void onClick(View v)
	{
		DialogGenerator dg = new DialogGenerator(this);

		switch(v.getId())
		{
		case R.id.chkStudent:
			if(isClickOrShowDialog()){
				type = "3";
				chkGrade.setBackgroundResource(grey_radio_btn_id);
				chkSchool.setBackgroundResource(grey_radio_btn_id);
				chkStudent.setBackgroundResource(green_radio_btn_id);
			}
			break;

		case R.id.chkSchool:
			if(isClickOrShowDialog()){
				type = "1";
				chkGrade.setBackgroundResource(grey_radio_btn_id);
				chkSchool.setBackgroundResource(green_radio_btn_id);
				chkStudent.setBackgroundResource(grey_radio_btn_id);
			}
			break;

		case R.id.chkGrade:
			if(isClickOrShowDialog()){
				type = "2";
				chkGrade.setBackgroundResource(green_radio_btn_id);
				chkSchool.setBackgroundResource(grey_radio_btn_id);
				chkStudent.setBackgroundResource(grey_radio_btn_id);
			}
			break;

		case R.id.imgInfo:
			dg.generateFriendzyWarningDialog(infoImg);
			break;

		case R.id.btnSchoolInfo:
			dg.generateFriendzyWarningDialog(infoschool);
			break;

		case R.id.btnStudentInfo:
			dg.generateFriendzyWarningDialog(infostud);
			break;

		case R.id.btnGradeInfo:
			dg.generateFriendzyWarningDialog(infograde);
			break;

		case R.id.btnSave:	
			onClickSaveRecord(dg);	

			break;

		case R.id.btnAddReward:
			Intent intent = new Intent(this, RewardActivity.class);
			startActivityForResult(intent, RESULT_FIRST_USER);
			break;

		case R.id.edtStartDate:
			if(isClickOrShowDialog()){
				Intent i = new Intent(this, CalenderView.class);
				startActivityForResult(i, START_DATE);
			}
			break;

		case R.id.edtEndDate:
			Intent intent2 = new Intent(this, CalenderView.class);
			startActivityForResult(intent2, END_DATE);
			break;

		}//End switch case

	}//END onClick method


	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent i) 
	{		
		if(resultCode == 1)
		{
			String date = i.getStringExtra("date");
			if(requestCode == START_DATE)
			{
				edtStartdate.setText(date);
			}
			if(requestCode == END_DATE)
			{
				edtEnddate.setText(date);
			}			
			checkStatus(edtStartdate.getText().toString(), edtEnddate.getText().toString());
		}
		if(requestCode == RESULT_FIRST_USER && resultCode == RESULT_OK)
		{
			friendzyObj.setRewards(i.getStringExtra("reward"));	
		}
		super.onActivityResult(requestCode, resultCode, i);
	}



	private void onClickSaveRecord(DialogGenerator dg)
	{	
		if(isStartDateValid && isEndDateValid && edtClassname.getText().length() > 1)
		{
			if(!challengerId.equals("0") && friendzyObj.getStatus().equals("1"))
			{
				friendzyObj.setClassName(edtClassname.getText()+"");
				friendzyObj.setEndDate(edtEnddate.getText()+"");
				friendzyObj.setChallengerId(challengerId);
			}
			else
			{
				friendzyObj.setClassName(edtClassname.getText()+"");
				friendzyObj.setEndDate(edtEnddate.getText()+"");
				friendzyObj.setStartDate(edtStartdate.getText()+"");
				friendzyObj.setGrade(spinGrade.getSelectedItem()+"");
				friendzyObj.setStatus(status);
				friendzyObj.setType(type);
				friendzyObj.setChallengerId(challengerId);
			}
			//Log.e("", ""+friendzyObj.getStatus()+" ,teachername "+friendzyObj.getTeacherName());

			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				new CreateChallenge(friendzyObj).execute(null,null,null);
			}
			else
			{
				CommonUtils.showInternetDialog(this);
			}
		}
		else if(edtClassname.getText().length() <= 1)
		{
			dg.generateWarningDialog(classAlert);
		}
		else if(!isStartDateValid)
		{
			dg.generateWarningDialog(startAlert);
		}
		else if(!isEndDateValid)
		{
			dg.generateWarningDialog(endAlert);
		}


	}//END onClickSaveRecord method


	@SuppressWarnings("deprecation")
	private void checkStatus(String date1, String date2)
	{
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy");
		try 
		{
			Date startDate = sdf.parse(date1);
			Date endDate   = sdf.parse(date2);
			Date todaydate = sdf.parse(currentDateandTime);

			if(startDate.equals(todaydate))
			{
				status = "1";
				isStartDateValid = true;
			}			
			else if((startDate.getDate() > todaydate.getDate() &&
					startDate.getMonth() >= todaydate.getMonth() &&
					startDate.getYear() >= todaydate.getYear()) || 
					(startDate.getDate() < todaydate.getDate() &&
							startDate.getMonth() > todaydate.getMonth() &&
							startDate.getYear() >= todaydate.getYear())){

				status = "2";
				isStartDateValid = true;
			}
			else
			{				
				isStartDateValid = false;
			}
			if((endDate.getTime()/(1000*24*60*60) - startDate.getTime()/(1000*24*60*60)) >= 1)
			{
				isEndDateValid   = true;
			}
			else
			{
				isEndDateValid  = false;
			}
			if((endDate.getTime()/(1000*24*60*60) - todaydate.getTime()/(1000*24*60*60)) < 1)
			{
				isEndDateValid   = false;
			}
		} catch (ParseException e) 
		{			
			e.printStackTrace();
		}

	}//END checkStatusAndType method


	private class CreateChallenge extends AsyncTask<Void, Void, Void>
	{
		FriendzyDTO friendzyDTO;
		ProgressDialog pd;	
		String challengeId;

		public CreateChallenge(FriendzyDTO friendzyDTO)
		{
			this.friendzyDTO = friendzyDTO;
			pd = CommonUtils.getProgressDialog(CreateChallengeActivity.this);
			pd.show();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			FriendzyServerOperation friendzy = new FriendzyServerOperation();
			challengeId = friendzy.createChallenge(friendzyDTO);
			return null;
		}


		@Override
		protected void onPostExecute(Void result) 
		{
			pd.cancel();
			friendzyObj.setChallengerId(challengeId);
			if(!challengerId.equals("0"))
			{
				Intent i = new Intent();
				i.putExtra("challengerId", challengerId);
				setResult(1, i);
				finish();
			}
			else
			{
				new GetChallengePid(friendzyDTO,challengeId).execute(null,null,null);
			}
			super.onPostExecute(result);
		}

	}

	private class GetChallengePid extends AsyncTask<Void, Void, Void>
	{
		FriendzyDTO friendzyDTO;
		String challengeId;
		String[] strPid; 		// at 0 index its for iPhone and at 1 its for android

		GetChallengePid(FriendzyDTO friendzyDTO, String challengeId)
		{
			this.friendzyDTO = friendzyDTO;
			this.challengeId = challengeId;
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			FriendzyServerOperation friendzy = new FriendzyServerOperation();
			strPid = friendzy.getDevicePid(challengeId);
			return null;
		}

		@Override
		protected void onPostExecute(Void result)
		{
			if(strPid[1] == null)
			{
				//Log.e("strPid", ""+strPid);
				Intent i = new Intent();
				i.putExtra("challengerId", challengerId);
				setResult(1, i);
				finish();
			}
			if(status.equals("1"))
			{
				new CreateChallengePid(friendzyDTO, strPid[1]).execute(null,null,null);
			}
			else
			{	
				//Log.e("status", ""+status);
				Intent i = new Intent();
				i.putExtra("challengerId", challengerId);
				setResult(1, i);
				finish();
			}
			super.onPostExecute(result);
		}

	}



	private class CreateChallengePid extends AsyncTask<Void, Void, Void>
	{
		FriendzyDTO friendzyDTO;
		String strPid;

		CreateChallengePid(FriendzyDTO friendzyDTO, String strPid)
		{
			this.friendzyDTO = friendzyDTO;
			this.strPid = strPid;
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			FriendzyServerOperation friendzy = new FriendzyServerOperation();
			friendzy.createNotificationForChallenge(friendzyDTO, strPid);
			return null;
		}

		@Override
		protected void onPostExecute(Void result)
		{
			Intent i = new Intent();
			i.putExtra("challengerId", challengerId);
			setResult(1, i);
			finish();
			super.onPostExecute(result);
		}

	}


	private class DeleteChallenge extends AsyncTask<Void, Void, Void>
	{
		ProgressDialog pd;	
		String challengeId;

		public DeleteChallenge(String challengeId)
		{
			this.challengeId = challengeId;
			pd = CommonUtils.getProgressDialog(CreateChallengeActivity.this);
			pd.show();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{
			FriendzyServerOperation friendzy = new FriendzyServerOperation();
			friendzy.deleteChallenge(challengeId);
			return null;
		}


		@Override
		protected void onPostExecute(Void result) 
		{
			pd.cancel();			
			Intent i = new Intent();
			i.putExtra("challengerId", "-1");
			setResult(1, i);
			finish();
			super.onPostExecute(result);
		}

	}

	/**
	 * Generate Delete confirmation dialog
	 * @param msg
	 */
	public void generateDeleteFriendzyChallengeDialog()
	{
		final Dialog dialog  = new Dialog(this, R.style.CustomDialogTheme);		
		dialog.setContentView(R.layout.dialogdeleteconfirmation);
		dialog.show();

		Button btnNoThanks 	= (Button)dialog.findViewById(R.id.btnDeleteNoThanks);
		Button btnOk 		= (Button)dialog.findViewById(R.id.btnDeletelOk);
		TextView txtMsg     = (TextView)dialog.findViewById(R.id.txtschoolAlertMessage);

		Translation transeletion = new Translation(this);		
		transeletion.openConnection();

		String msg = transeletion.getTranselationTextByTextIdentifier("alertMsgAreYouSureToDeleteTheChallenge");
		btnNoThanks.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleNoThanks"));
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		transeletion.closeConnection();

		txtMsg.setText(msg);

		btnNoThanks.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();				
			}
		});

		btnOk.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) 
			{
				dialog.dismiss();	
				if(CommonUtils.isInternetConnectionAvailable(CreateChallengeActivity.this))
				{
					new DeleteChallenge(challengerId).execute(null,null,null);
				}
				else
				{
					CommonUtils.showInternetDialog(CreateChallengeActivity.this);
				}

			}

		});
	}


	//Show PopUpFor Active Challenge
	private boolean isClickOrShowDialog(){

		if(!challengerId.equals("0") && friendzyObj.getStatus().equals("1"))
		{
			DialogGenerator dg = new DialogGenerator(this);		
			dg.generateWarningDialog(activeMsg);

			return false;
		}else{
			return true;
		}
	}

}

package com.mathfriendzy.controller.registration;

import static com.mathfriendzy.utils.ICommonUtils.PRIVACY_POLICY_URL;
import static com.mathfriendzy.utils.ICommonUtils.REGISTRATION_STEP_1_FALG;

import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.forthgradepaid.R;
import com.mathfriendzy.controller.base.RegistartionBase;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.registration.newregistration.ParentRegistrationStep2;
import com.mathfriendzy.controller.registration.newregistration.StudentRegistrationStep2;
import com.mathfriendzy.controller.registration.newregistration.TeacherRegistrationStep2;
import com.mathfriendzy.controller.webview.PrivacyPolicyActivity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.listener.EditTextFocusChangeListener;
import com.mathfriendzy.listener.OnRequestCompleteWithStringResult;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.player.temp.TempPlayer;
import com.mathfriendzy.model.player.temp.TempPlayerOperation;
import com.mathfriendzy.model.registration.Registration;
import com.mathfriendzy.social.google.GoogleProfile;
import com.mathfriendzy.social.google.MyGoogleCallBack;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;

/**
 * This Activity is open when user want to registered
 * This is First Step Activity
 * @author Yashwant Singh
 *
 */
public class RegistrationStep1 extends RegistartionBase implements OnClickListener , MyGoogleCallBack , EditTextFocusChangeListener
{
    private TextView mfTitleHomeScreen 				= null;
    private TextView regTitleRegistration 			= null;
    private TextView lblCreateFreeAccountToAddPlayers= null;
    private TextView regTextAreYouA 				= null;
    private TextView mfBtnTitleStudents 			= null;
    private TextView lblRegParent 					= null;
    private TextView lblRegTeacher					= null;
    private TextView lblFirstName 					= null;
    private TextView lblLastName 					= null;
    private TextView lblRegEmail 					= null;
    private TextView lblRegPassword 				= null;

    private Button btnTitleSubmit 						= null;
    private TextView lblBySelectingSubmitYouAcknowledge = null;
    private TextView lblPrivacyPolicyAndTermsOfService 	= null;

    private ImageButton RegS1ImgBtnParent			= null;
    private ImageButton RegS1ImgBtnTeacher 			= null;
    private ImageButton RegS1ImgBtnStudent 			= null;
    private EditText edtFirstName 					= null;
    private EditText edtLastName 					= null;
    private EditText edtEmail 						= null;
    private EditText edtPassword 					= null;
    private Button   btnTitleBack      				= null;
    private EditText edtUsername                    = null;

    private boolean isTeacher = false;
    private boolean isStudent = false;
    private boolean isParent  = true;

    private final String TAG = this.getClass().getSimpleName();


    //for point no. in 18 Apr 2014 Sheet.
    private TextView txtKhanAcademyText = null;

    //for google connect
    //private SignInButton btnLoginWithGoogle = null;
    private Button btnLoginWithGoogle = null;
    //private GoogleConnect googleConnect = null;
    private boolean isSignInWithGoogle = false;


    //for the new updated UI button
    private RelativeLayout studentLayout = null;
    private RelativeLayout parentLayout  = null;
    private RelativeLayout teacherLayout = null;

    //for account type
    private final int TEACHER = 1;
    private final int STUDENT = 2;
    private final int PARENT  = 3;

    private GoogleProfile profile = null;
    private boolean isRegisterFromGoogle = false;
    private String alertPleaseRegisterBeforeLogin = "Please register before login.";

    private EditText edtConfirmEmail = null;
    private String alertMsgEmailMismatch = null;

    private String lblEnterEmailToReceiveWeeklyReport = "Enter an email to receive weekly progress reports.";
    private boolean onFocusLostIsValidString = true;
    private String lblOnlyLastInitial = "Only the last initial is allowed";
    private String lblLastInitial = "Last Initial";
    private String txtLastName = "Last Name";    

    private boolean isOpenForInAppPurchase = false;

    @Override
    protected void onCreate(Bundle savedInstanceState){

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration_step1);

        if(REGISTRATION_STEP_1_FALG)
            Log.e(TAG, "inside onCreate()");

        //googleConnect = new GoogleConnect(this);
        //googleConnect.initializeGoogleClient();

        MathFriendzyHelper.initializeProcessDialog(this);

        this.getIntentValues();
        this.setWidgetsReferences();
        this.setWidgetsTextValues();
        this.setListenerOnWidgets();
        this.setDataFromGoogle();
        this.showRegistrationDilog();

        this.clickOnStudentTab();//By Default Student tab selected
        
        //Hide the teacher layout
        this.setVisibilityOfTeacherButton();
        
        if(REGISTRATION_STEP_1_FALG)
            Log.e(TAG, "outside onCreate()");
    }

    private void setVisibilityOfTeacherButton(){
    	teacherLayout.setVisibility(RelativeLayout.GONE);
    }
    
    /**
     * Get the intent values
     */
    private void getIntentValues(){
        isRegisterFromGoogle = this.getIntent().getBooleanExtra("isRegisterFromGoogle", false);
        if(isRegisterFromGoogle){
            profile = (GoogleProfile) this.getIntent().getSerializableExtra("googleProfileData");
        }
        
        isOpenForInAppPurchase = this.getIntent().getBooleanExtra("isOpenForInAppPurchase" , false);
    }

    /**
     * Set the data when registered from google
     */
    private void setDataFromGoogle() {
        try{
            if(isRegisterFromGoogle && profile != null){
                isSignInWithGoogle = true;
                this.setDataFromGoogle(profile);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    /**
     * Show dialog when user want to login with google and registered before
     */
    private void showRegistrationDilog(){
        if(isRegisterFromGoogle){
            MathFriendzyHelper.warningDialog(this, alertPleaseRegisterBeforeLogin);
        }
    }

    /**
     * This method set the widgets references from the Layout to the widgets Objects
     */
    protected void setWidgetsReferences()
    {
        if(REGISTRATION_STEP_1_FALG)
            Log.e(TAG, "inside setWidgetsReferences()");

        mfTitleHomeScreen 					= (TextView) findViewById(R.id.mfTitleHomeScreen);
        regTitleRegistration				= (TextView) findViewById(R.id.regTitleRegistration);
        lblCreateFreeAccountToAddPlayers 	= (TextView) findViewById(R.id.lblCreateFreeAccountToAddPlayers);
        regTextAreYouA 						= (TextView) findViewById(R.id.regTextAreYouA);
        mfBtnTitleStudents 					= (TextView) findViewById(R.id.mfBtnTitleStudents);
        lblRegParent 						= (TextView) findViewById(R.id.lblRegParent);
        lblRegTeacher 						= (TextView) findViewById(R.id.lblRegTeacher);
        lblFirstName 						= (TextView) findViewById(R.id.lblFirstName);
        lblLastName 						= (TextView) findViewById(R.id.lblLastName);
        lblRegEmail 						= (TextView) findViewById(R.id.lblRegEmail);
        lblRegPassword 						= (TextView) findViewById(R.id.lblRegPassword);
        lblBySelectingSubmitYouAcknowledge 	= (TextView) findViewById(R.id.lblBySelectingSubmitYouAcknowledge);
        btnTitleSubmit 						= (Button) findViewById(R.id.btnTitleSubmit);
        lblPrivacyPolicyAndTermsOfService   = (TextView) findViewById(R.id.lblPrivacyPolicyAndTermsOfService);

        RegS1ImgBtnParent 					= (ImageButton) findViewById(R.id.RegS1ImgBtnParent);
        RegS1ImgBtnTeacher 					= (ImageButton) findViewById(R.id.RegS1ImgBtnTeacher);
        RegS1ImgBtnStudent 					= (ImageButton) findViewById(R.id.RegS1ImgBtnStudent);

        edtFirstName 						= (EditText) findViewById(R.id.edtFirstName);
        edtLastName 						= (EditText) findViewById(R.id.edtLastName);
        edtEmail 							= (EditText) findViewById(R.id.edtEmail);
        edtPassword 						= (EditText) findViewById(R.id.edtPassword);
        btnTitleBack            			= (Button)   findViewById(R.id.btnTitleBack);
        //for new registration
        edtUsername                         = (EditText) findViewById(R.id.edtUsername);

        //for point no. in 18 Apr 2014 Sheet.
        txtKhanAcademyText                  = (TextView) findViewById(R.id.txtKhanAcademyText);

        btnLoginWithGoogle = (Button) findViewById(R.id.btnLoginWithGoogle);
        edtConfirmEmail = (EditText) findViewById(R.id.edtConfirmEmail);

        //set Done button to keyboard
        MathFriendzyHelper.setDoneButtonToEditText(edtFirstName);
        MathFriendzyHelper.setDoneButtonToEditText(edtLastName);
        MathFriendzyHelper.setDoneButtonToEditText(edtEmail);
        MathFriendzyHelper.setDoneButtonToEditText(edtPassword);
        MathFriendzyHelper.setDoneButtonToEditText(edtConfirmEmail);
        MathFriendzyHelper.setDoneButtonToEditText(edtUsername);
        //end changes

        //for the new UI changes
        studentLayout = (RelativeLayout) findViewById(R.id.studentLayout);
        parentLayout = (RelativeLayout) findViewById(R.id.parentLayout);
        teacherLayout = (RelativeLayout) findViewById(R.id.teacherLayout);

        txtKhanAcademyText.setVisibility(TextView.GONE);

        if(REGISTRATION_STEP_1_FALG)
            Log.e(TAG, "outside setWidgetsReferences()");
    }

    /**
     * This method set the Widgets Text Values from tha translation
     */
    private void setWidgetsTextValues()
    {
        if(REGISTRATION_STEP_1_FALG)
            Log.e(TAG, "inside setWidgetsTextValues()");

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        mfTitleHomeScreen.setText(MathFriendzyHelper.getAppName(transeletion));
        regTitleRegistration.setText(transeletion.getTranselationTextByTextIdentifier("lblQuickRegistration"));
        lblCreateFreeAccountToAddPlayers.setText(transeletion.getTranselationTextByTextIdentifier("lblCreateFreeAccountToAddPlayers"));
        regTextAreYouA.setText(transeletion.getTranselationTextByTextIdentifier("regTextAreYouA") + ":");
        mfBtnTitleStudents.setText(transeletion.getTranselationTextByTextIdentifier("lblStudent"));
        lblRegParent.setText(transeletion.getTranselationTextByTextIdentifier("lblRegParent") );
        lblRegTeacher.setText(transeletion.getTranselationTextByTextIdentifier("lblRegTeacher") );
        lblFirstName.setText(transeletion.getTranselationTextByTextIdentifier("lblFirstName") + ":");
        this.setHintToEditText(edtFirstName, transeletion.getTranselationTextByTextIdentifier("lblFirstName"));
        lblLastName.setText(transeletion.getTranselationTextByTextIdentifier("lblLastInitial") + ":");
        this.setHintToEditText(edtLastName , transeletion.getTranselationTextByTextIdentifier("lblLastInitial"));
        lblRegEmail.setText(transeletion.getTranselationTextByTextIdentifier("lblRegEmail") + ":");
        this.setHintToEditText(edtEmail, transeletion.getTranselationTextByTextIdentifier("lblRegEmail"));
        this.setHintToEditText(edtConfirmEmail, transeletion
                .getTranselationTextByTextIdentifier("lblRegEmailConfirm"));
        lblRegPassword.setText(transeletion.getTranselationTextByTextIdentifier("lblRegPassword"));
        this.setHintToEditText(edtPassword, transeletion.getTranselationTextByTextIdentifier("lblRegPassword"));
        this.setHintToEditText(edtUsername, transeletion.getTranselationTextByTextIdentifier("lblUserName"));
        lblBySelectingSubmitYouAcknowledge.setText(transeletion.getTranselationTextByTextIdentifier("lblBySelectingSubmitYouAcknowledge"));
        btnTitleSubmit.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleSubmit") );
        lblPrivacyPolicyAndTermsOfService.setText(Html.fromHtml(MathFriendzyHelper
                .convertStringToUnderLineString(transeletion
                        .getTranselationTextByTextIdentifier("lblPrivacyPolicyAndTermsOfService"))));
        btnTitleBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));

        //for point no. in 18 Apr 2014 Sheet.
        txtKhanAcademyText.setText(Html.fromHtml(this.getKhanVedioText(transeletion.getTranselationTextByTextIdentifier("lblKhanAcademyVideos"))));

        alertPleaseRegisterBeforeLogin = transeletion.
                getTranselationTextByTextIdentifier("lblPleaseRegisterBeforeLogin");
        alertMsgEmailMismatch = transeletion.
                getTranselationTextByTextIdentifier("alertMsgEmailMismatch");
        lblEnterEmailToReceiveWeeklyReport = transeletion.
                getTranselationTextByTextIdentifier("lblEnterEmailToReceiveWeeklyReport");        
        
        lblLastInitial = transeletion.getTranselationTextByTextIdentifier("lblLastInitial");
        txtLastName = transeletion.getTranselationTextByTextIdentifier("lblLastName");
        lblOnlyLastInitial = transeletion.getTranselationTextByTextIdentifier("lblOnlyLastInitial");
        transeletion.closeConnection();

        if(REGISTRATION_STEP_1_FALG)
            Log.e(TAG, "outside setWidgetsTextValues()");
    }

    /**
     * This method create the html text
     * @param text
     * @return
     */
    private String getKhanVedioText(String text){
        try {
            if (!MathFriendzyHelper.isEmpty(text)) {
                String htmlText = "<html>" + text.subSequence(0, text.indexOf("w"))
                        + "<font color='blue'>" + text.substring(text.indexOf("w")) + "</font></html>";
                return htmlText;
            } else {
                return "";
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return "";
    }

    /**
     * This method set listener on widgets
     */
    protected void setListenerOnWidgets()
    {
        if(REGISTRATION_STEP_1_FALG)
            Log.e(TAG, "inside setListenerOnWidgets()");

        RegS1ImgBtnParent.setOnClickListener(this);
        RegS1ImgBtnTeacher.setOnClickListener(this);
        RegS1ImgBtnStudent.setOnClickListener(this);
        btnTitleSubmit.setOnClickListener(this);
        lblPrivacyPolicyAndTermsOfService.setOnClickListener(this);
        btnTitleBack.setOnClickListener(this);

        //for point no. in 18 Apr 2014 Sheet.
        txtKhanAcademyText.setOnClickListener(this);
        btnLoginWithGoogle.setOnClickListener(this);

        //for new UI changes
        studentLayout.setOnClickListener(this);
        parentLayout.setOnClickListener(this);
        teacherLayout.setOnClickListener(this);

        MathFriendzyHelper.setFocusChangeListener(this , edtFirstName , this);
        MathFriendzyHelper.setFocusChangeListener(this , edtLastName , this);
        MathFriendzyHelper.setFocusChangeListener(this , edtEmail , this);
        MathFriendzyHelper.setFocusChangeListener(this , edtConfirmEmail , this);
        MathFriendzyHelper.setFocusChangeListener(this , edtUsername , this);
        MathFriendzyHelper.setFocusChangeListener(this , edtPassword , this);

        //this.setTextWatcherToLastName();

        //MathFriendzyHelper.setEditTextWatcherToEditTextForLastInitialName(this , edtLastName);
        edtLastName.addTextChangedListener(textWatcher);
        
        if(REGISTRATION_STEP_1_FALG)
            Log.e(TAG, "outside setListenerOnWidgets()");
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.btnTitleSubmit:
                MathFriendzyHelper.clearFocus(this);
                if(onFocusLostIsValidString)
                    this.checkEmptyValidation();
               /* }else{
                    MathFriendzyHelper.warningDialog(this , alertMsgEmailMismatch);
                }*/
                break;

            case R.id.RegS1ImgBtnStudent:
                this.clickOnStudentTab();
                break;

            case R.id.RegS1ImgBtnParent:
                this.clickOnParentTab();
                break;

            case R.id.RegS1ImgBtnTeacher:
                this.clickOnTeacherTab();
                break;

            case R.id.lblPrivacyPolicyAndTermsOfService:
                //changes for Interner
                if(CommonUtils.isInternetConnectionAvailable(this))
                {
                    Intent intentWeb = new Intent(this,PrivacyPolicyActivity.class);
                    intentWeb.putExtra("url", PRIVACY_POLICY_URL);
                    startActivity(intentWeb);
                }
                else
                {
                    DialogGenerator dg = new DialogGenerator(this);
                    Translation transeletion = new Translation(this);
                    transeletion.openConnection();
                    dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
                    transeletion.closeConnection();
                }
                break;

            case R.id.btnTitleBack:
            	this.openMainActivity();
                break;
            //for point no. in 18 Apr 2014 Sheet.
            case R.id.txtKhanAcademyText:
                startActivity(new Intent(Intent.ACTION_VIEW , Uri.parse("http://www.KhanAcademy.com")));
                break;
            case R.id.btnLoginWithGoogle:
                this.signInWithGoogle();
                break;
            case R.id.studentLayout:
                this.clickOnStudentTab();
                break;
            case R.id.parentLayout:
                this.clickOnParentTab();
                break;
            case R.id.teacherLayout:
                this.clickOnTeacherTab();
                break;
            default:
                break;
        }
    }

    /**
     * Change the selection type account background
     * @param accountFor
     */
    private void setBackToSelectedAccount(int accountFor){
        if(accountFor == TEACHER){
            studentLayout.setBackgroundResource(R.drawable.grey_button_new_registration);
            teacherLayout.setBackgroundResource(R.drawable.blue_button_new_registration);
            parentLayout.setBackgroundResource(R.drawable.grey_button_new_registration);
        }else if(accountFor == STUDENT){
            studentLayout.setBackgroundResource(R.drawable.blue_button_new_registration);
            teacherLayout.setBackgroundResource(R.drawable.grey_button_new_registration);
            parentLayout.setBackgroundResource(R.drawable.grey_button_new_registration);
        }else if(accountFor == PARENT){
            studentLayout.setBackgroundResource(R.drawable.grey_button_new_registration);
            teacherLayout.setBackgroundResource(R.drawable.grey_button_new_registration);
            parentLayout.setBackgroundResource(R.drawable.blue_button_new_registration);
        }
    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event)
    {
        if(keyCode == KeyEvent.KEYCODE_BACK)
        {
            this.openMainActivity();
            return false;
        }
        return super.onKeyDown(keyCode, event);
    }

    /**
     * This method set the tempplayer first and last name when user registered ad a student, if temp player exist
     */
    private void setTextValuesFromTempPlayer(){
        if(!isSignInWithGoogle){
            TempPlayerOperation tempPlayerObj = new TempPlayerOperation(this);
            if(tempPlayerObj.isTemparyPlayerExist()){
                ArrayList<TempPlayer> templayer = tempPlayerObj.getTempPlayerData();
                if(templayer.size() > 0){
                    if(isStudent) {
                        edtFirstName.setText(templayer.get(0).getFirstName());
                        edtLastName.setText(MathFriendzyHelper
                                .getLastInitialName(templayer.get(0).getLastName()));
                    }else{
                        edtFirstName.setText("");
                        edtLastName.setText("");
                    }
                }
                /*else{
                    edtFirstName.setText("");
                    edtLastName.setText("");
                }*/
            }else{
                tempPlayerObj.closeConn();
                /*edtFirstName.setText("");
                edtLastName.setText("");*/
            }
        }
    }

    /**
     * Set emplty text when user registered as parent or teacher
     */
    private void setEmptyText(){
        if(!isSignInWithGoogle){
            edtFirstName.setText("");
            edtLastName.setText("");
        }
    }

    /**
     * Set hint to edit text
     * @param edt
     * @param hint
     */
    protected void setHintToEditText(EditText edt , String hint){
        //Log.e(TAG, "edt " + edt + " hint " + hint);
        edt.setHint(hint);
    }

    /**
     * Set email text
     */
    private void setEmailText(int value)
    {
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        if(value == 1)//1 for registered as student
        {
            /*lblRegEmail.setText(transeletion.getTranselationTextByTextIdentifier("lblRegEmail") + "(" +
                    transeletion.getTranselationTextByTextIdentifier("lblOptional") + ")" +  ":");
            this.setHintToEditText(edtEmail, transeletion.getTranselationTextByTextIdentifier("lblRegEmail") + "(" +
                    transeletion.getTranselationTextByTextIdentifier("lblOptional") + ")");*/
            lblRegEmail.setText(lblEnterEmailToReceiveWeeklyReport);
            this.setHintToEditText(edtEmail, lblEnterEmailToReceiveWeeklyReport /*+ "(" +
                    transeletion.getTranselationTextByTextIdentifier("lblOptional") + ")"*/);
        }
        else
        {
            lblRegEmail.setText(transeletion.getTranselationTextByTextIdentifier("lblRegEmail") + ":");
            this.setHintToEditText(edtEmail, transeletion.getTranselationTextByTextIdentifier("lblRegEmail"));
        }
        transeletion.closeConnection();
    }

    /**
     * Check for email existence
     */
    private void checkEmailExistence(){
        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        DialogGenerator dg = new DialogGenerator(this);
        if(CommonUtils.isEmailValid(edtEmail.getText().toString())){
            if(CommonUtils.isPasswordValid(edtPassword.getText().toString())){
                //changes for Internet Connection
                if(CommonUtils.isInternetConnectionAvailable(this))	{
                    new CheckEmailExistence(edtEmail.getText().toString()).execute(null,null,null);
                }
                else{
                    dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
                }
            }else{
                dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPasswordInvalid"));
            }
        }else{
            dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgEmailIncorrectFormat"));
        }
        transeletion.closeConnection();
    }

    private RegistrationStep1 getCurrentObj(){
        return this;
    }

    /**
     * This method check for empty validation before going to the next step
     */
    private void checkEmptyValidation()
    {
        if(REGISTRATION_STEP_1_FALG)
            Log.e(TAG, "inside checkEmptyValidation()");

        //new change to remove email confirmation from the UI
        edtConfirmEmail.setText(edtEmail.getText().toString());
        //end new change

        Translation transeletion = new Translation(this);
        transeletion.openConnection();
        DialogGenerator dg = new DialogGenerator(this);

        //if(edtEmail.getText().toString().length() > 0)
        if(!isStudent)
        {
            if(edtFirstName.getText().toString().equals("")
                    ||edtLastName.getText().toString().equals("")
                    ||edtEmail.getText().toString().equals("")
                    ||edtPassword.getText().toString().equals("")
                    ||edtUsername.getText().toString().equals(""))
            {
                dg.generateWarningDialog(transeletion.
                        getTranselationTextByTextIdentifier("alertMsgPleaseEnterAllInfo"));
            }
            else{

                if(!(edtEmail.getText().toString()
                        .equals(edtConfirmEmail.getText().toString()))){
                    MathFriendzyHelper.warningDialog(this , alertMsgEmailMismatch);
                    return;
                }

                String userName = edtUsername.getText().toString();
                MathFriendzyHelper.showDialog();
                MathFriendzyHelper.checkForValidUserName(userName,
                        new OnRequestCompleteWithStringResult() {

                            @Override
                            public void onComplete(String result) {
                                MathFriendzyHelper.dismissDialog();
                                if(result != null){
                                    if(result.equals("1")){//1 for already exist user name
                                        MathFriendzyHelper.showAlreadyExistUserNameDialog
                                                (getCurrentObj());
                                    }else{
                                        checkEmailExistence();
                                    }
                                }else{
                                    CommonUtils.showInternetDialog(getCurrentObj());
                                }
                            }
                        });
                //this.checkEmailExistence(transeletion, dg);
            }
        }
        else
        {
            if(edtFirstName.getText().toString().equals("")
                    ||edtLastName.getText().toString().equals("")
                    ||edtPassword.getText().toString().equals("")
                    ||edtUsername.getText().toString().equals("")){
                dg.generateWarningDialog(transeletion
                        .getTranselationTextByTextIdentifier("alertMsgPleaseEnterAllInfo"));
            }
            else{

                if(!(edtEmail.getText().toString()
                        .equals(edtConfirmEmail.getText().toString()))){
                    MathFriendzyHelper.warningDialog(this , alertMsgEmailMismatch);
                    return;
                }

                if(CommonUtils.isPasswordValid(edtPassword.getText().toString())){
                    String userName = edtUsername.getText().toString();
                    MathFriendzyHelper.showDialog();
                    MathFriendzyHelper.checkForValidUserName(userName,
                            new OnRequestCompleteWithStringResult() {
                                @Override
                                public void onComplete(String result) {
                                    MathFriendzyHelper.dismissDialog();
                                    if(result != null){
                                        if(result.equals("1")){//1 for already exist user name
                                            MathFriendzyHelper.showAlreadyExistUserNameDialog
                                                    (getCurrentObj());
                                        }else{
                                            if(MathFriendzyHelper
                                                    .isEmpty(edtEmail.getText().toString())){
                                                goForRegistrationStep2();
                                            }else{
                                                checkEmailExistence();
                                            }
                                        }
                                    }else{
                                        CommonUtils.showInternetDialog(getCurrentObj());
                                    }
                                }
                            });
                }else{
                    dg.generateWarningDialog(transeletion.
                            getTranselationTextByTextIdentifier("alertMsgPasswordInvalid"));
                }
                //goForRegistrationStep2();
            }
        }
        transeletion.closeConnection();

        if(REGISTRATION_STEP_1_FALG)
            Log.e(TAG, "outside checkEmptyValidation()");
    }

    /**
     * @description Check for email is already registered or not on server
     * @author Yashwant Singh
     *
     */
    class CheckEmailExistence extends AsyncTask<Void, Void, Void>
    {
        private String email 			= null;
        ProgressDialog progressDialog 	= null;
        private boolean isEmailexist    = false;

        CheckEmailExistence(String email)
        {
            this.email = email;
        }
        @Override
        protected void onPreExecute()
        {
            if(REGISTRATION_STEP_1_FALG)
                Log.e(TAG, "inside CheckEmailExistence onPreExecute()");

            progressDialog = CommonUtils.getProgressDialog(RegistrationStep1.this);
            progressDialog.show();

            if(REGISTRATION_STEP_1_FALG)
                Log.e(TAG, "outside CheckEmailExistence onPreExecute()");
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params)
        {
            if(REGISTRATION_STEP_1_FALG)
                Log.e(TAG, "inside CheckEmailExistence doInBackground()");

            CommonUtils commomObj = new CommonUtils();
            isEmailexist = commomObj.isEmailAlreadyExist(email);

            if(REGISTRATION_STEP_1_FALG)
                Log.e(TAG, "outside CheckEmailExistence doInBackground()");

            return null;
        }

        @Override
        protected void onPostExecute(Void result)
        {
            if(REGISTRATION_STEP_1_FALG)
                Log.e(TAG, "inside CheckEmailExistence onPostExecute()");

            progressDialog.cancel();

            if(!isEmailexist)
            {
				/*Intent intent = new Intent(RegistrationStep1.this,RegisterationStep2.class);
				intent.putExtra("fName", edtFirstName.getText().toString());
				intent.putExtra("lName", edtLastName.getText().toString());
				intent.putExtra("email", edtEmail.getText().toString());
				intent.putExtra("pass", edtPassword.getText().toString());
				intent.putExtra("isTeacher", isTeacher);
				intent.putExtra("isStudent", isStudent);
				intent.putExtra("isParent", isParent);
				startActivity(intent);*/
                goForRegistrationStep2();
            }
            else
            {
                Translation transeletion = new Translation(RegistrationStep1.this);
                transeletion.openConnection();
                DialogGenerator dg = new DialogGenerator(RegistrationStep1.this);
                dg.generateDialogEmailExistDialog
                        (transeletion.getTranselationTextByTextIdentifier("alertMsgEmailExist"));
                transeletion.closeConnection();
            }

            if(REGISTRATION_STEP_1_FALG)
                Log.e(TAG, "inside CheckEmailExistence onPostExecute()");

            super.onPostExecute(result);
        }
    }


    /**
     * Set the registration first step Data
     * @return
     */
    private Registration getRegistrationStep1(){
        Registration registration = new Registration();
        registration.setfName(edtFirstName.getText().toString());
        registration.setlName(edtLastName.getText().toString());
        registration.setEmail(edtEmail.getText().toString());
        registration.setPassword(edtPassword.getText().toString());
        registration.setUserName(edtUsername.getText().toString());
        registration.setParent(isParent);
        registration.setStudent(isStudent);
        registration.setTeacher(isTeacher);
        return registration;
    }

    /**
     * Go for registration step 2
     */
    private void goForRegistrationStep2(){
        try{
            Intent intent = null;
            if(isParent){
                intent = new Intent(RegistrationStep1.this,ParentRegistrationStep2.class);
				/*MathFriendzyHelper.registereUser(this, MathFriendzyHelper.UNITED_STATE, 
						MathFriendzyHelper.ADULT_GRADE, MathFriendzyHelper.DEFAULT_US_ZIP, 
						MathFriendzyHelper.getDefaultSchool(), MathFriendzyHelper.getDefaultTeacher(), "1",
						MathFriendzyHelper.getTempPlayer(this), this.getRegistrationStep1(), 
						MathFriendzyHelper.DEFAULT_PROFILE_IMAGE);*/
            }else if(isStudent){
                intent = new Intent(RegistrationStep1.this,StudentRegistrationStep2.class);
            }else if(isTeacher){
                intent = new Intent(RegistrationStep1.this,TeacherRegistrationStep2.class);
            }
            intent.putExtra("registrationStep1", this.getRegistrationStep1());
            startActivity(intent);
        }catch(Exception e){
            e.printStackTrace();
        }
    }    

    /**
     * Set tha data from Google
     * @param profile
     */
    private void setDataFromGoogle(GoogleProfile profile){
        try{
            edtFirstName.setText(profile.getfName());
            edtLastName.setText(MathFriendzyHelper.getLastInitialName(profile.getlName()));
            edtEmail.setText(profile.getEmail());
            edtUsername.setText(profile.getUserName());
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    @Override
    protected void onStart() {
        //googleConnect.onStart();
        super.onStart();
    }

    @Override
    protected void onStop() {
        //googleConnect.signOut();
        super.onStop();
    }

    //Register with Google
    private void signInWithGoogle(){
        if(!isSignInWithGoogle) {
            //googleConnect.signIn();
        }else
            MathFriendzyHelper.warningDialog
                    (this, "You are already login...");
    }

    @Override
    public void onProfileData(GoogleProfile profile) {
        isSignInWithGoogle = true;
        this.setDataFromGoogle(profile);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        try{
            //googleConnect.onActivityResultCall(requestCode, resultCode, intent);
        }catch(Exception e){
            e.printStackTrace();
        }
        super.onActivityResult(requestCode, resultCode, intent);
    }


    //check only for the last edittext which has current focus
    @Override
    public void onFocusLost(boolean isValidString) {
        onFocusLostIsValidString = isValidString;
    }

    @Override
    public void onFocusHas(boolean isValidString) {

    }

    private void clickOnStudentTab(){
        RegS1ImgBtnStudent.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
        RegS1ImgBtnParent.setBackgroundResource(R.drawable.mf_check_box_ipad);
        RegS1ImgBtnTeacher.setBackgroundResource(R.drawable.mf_check_box_ipad);
                /*lblBySelectingSubmitYouAcknowledge.setVisibility(TextView.GONE);
                lblPrivacyPolicyAndTermsOfService.setVisibility(TextView.GONE);*/
        lblBySelectingSubmitYouAcknowledge.setVisibility(TextView.VISIBLE);
        lblPrivacyPolicyAndTermsOfService.setVisibility(TextView.VISIBLE);
        this.setEmailText(1);
        this.setBackToSelectedAccount(STUDENT);
        isTeacher = false;
        isStudent = true;
        isParent  = false;
        this.setTextValuesFromTempPlayer();
        
        this.setHintAndListenerOnLastName(STUDENT);
    }

    private void clickOnParentTab() {
        RegS1ImgBtnStudent.setBackgroundResource(R.drawable.mf_check_box_ipad);
        RegS1ImgBtnParent.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
        RegS1ImgBtnTeacher.setBackgroundResource(R.drawable.mf_check_box_ipad);
        lblBySelectingSubmitYouAcknowledge.setVisibility(TextView.VISIBLE);
        lblPrivacyPolicyAndTermsOfService.setVisibility(TextView.VISIBLE);
        this.setEmailText(0);
        this.setBackToSelectedAccount(PARENT);
        isTeacher = false;
        isStudent = false;
        isParent  = true;
        this.setTextValuesFromTempPlayer();
        
        this.setHintAndListenerOnLastName(PARENT);
    }

    private void clickOnTeacherTab(){
        RegS1ImgBtnStudent.setBackgroundResource(R.drawable.mf_check_box_ipad);
        RegS1ImgBtnParent.setBackgroundResource(R.drawable.mf_check_box_ipad);
        RegS1ImgBtnTeacher.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
        lblBySelectingSubmitYouAcknowledge.setVisibility(TextView.VISIBLE);
        lblPrivacyPolicyAndTermsOfService.setVisibility(TextView.VISIBLE);
        this.setEmailText(0);
        this.setBackToSelectedAccount(TEACHER);
        isTeacher = true;
        isStudent = false;
        isParent  = false;
        this.setTextValuesFromTempPlayer();
        
        this.setHintAndListenerOnLastName(TEACHER);
    }
    
    private void setHintAndListenerOnLastName(int accountType) {
        if (accountType == TEACHER || accountType == PARENT) {
            edtLastName.setHint(txtLastName);
        } else {
            edtLastName.setHint(lblLastInitial);
            edtLastName.setText(MathFriendzyHelper.getLastInitialName(edtLastName.getText().toString()));
        }
    }

    TextWatcher textWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String text = String.valueOf(s);
            if(isStudent) {
                if (text.length() > 1) {
                    edtLastName.setText(MathFriendzyHelper.getLastInitialName(text));
                    MathFriendzyHelper.showWarningDialog(RegistrationStep1.this, 
                    		lblOnlyLastInitial);
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {

        }
    };

    private void openMainActivity(){
        if(isOpenForInAppPurchase){
            Intent intent = new Intent();
            setResult(RESULT_OK, intent);
            finish();
            return ;
        }
        Intent intentMain = new Intent(this, MainActivity.class);
        startActivity(intentMain);
    }
}


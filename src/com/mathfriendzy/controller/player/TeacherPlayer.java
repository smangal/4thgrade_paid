package com.mathfriendzy.controller.player;

import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_USER_PLAYER_FLAG;

import java.util.ArrayList;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.forthgradepaid.R;
import com.mathfriendzy.controller.friendzy.TeacherChallengeActivity;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.teacherStudents.TeacherStudents;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.customview.DynamicLayout;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.registration.UserPlayerDto;
import com.mathfriendzy.model.registration.UserPlayerOperation;
import com.mathfriendzy.utils.DialogGenerator;
import com.mathfriendzy.utils.ITextIds;

/**
 * this Acitvity shows the player when a user is login as a teacher
 * @author Yashwant Singh
 *
 */
public class TeacherPlayer extends AdBaseActivity implements OnClickListener
{
	private TextView mfTitleHomeScreen 		= null;
	private TextView lblPleaseSelectPlayer 	= null;
	private TextView textResult  			= null;
	private TextView btnTitleEdit 			= null;
	private Button   lblAddPlayer 			= null;
	private Button   playTitle 				= null;
	private Button   btnTitleTop100         = null;
	private TextView btnTitlePlayers        = null;
	private Button   myStudents             = null;

	//private ListView playerListView 		= null;
	private ArrayList<UserPlayerDto> userPlayerList   = null;
	private LinearLayout linearLayout = null;

	private final String TAG = this.getClass().getSimpleName();
	private Button   btnTitleBack       = null;
	private String callingActivity		= " ";

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_teacher_player);

		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " inside onCreate()");

		callingActivity = getIntent().getStringExtra("calling");
		if(callingActivity == null)
		{
			callingActivity = "Main";
		}

		this.setWidgetsReferences();
		this.setWidgetsTextValue();
		this.getUserPlayersData();
		this.setListenerOnWidgets();

		/*UserPlayersAdapter adapter = new UserPlayersAdapter(this,R.layout.list_players,userPlayerList);
		playerListView.setAdapter(adapter);*/
		//this.createDyanamicLayoutForDisplayUserPlayer();
		DynamicLayout dynamicLayout = new DynamicLayout(this);
		dynamicLayout.createDyanamicLayoutForDisplayUserPlayer(userPlayerList, linearLayout, false);

		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " outside onCreate()");
	}

	/**
	 * This method set the wodgets refernces from the layout to the widgets objects
	 */
	private void setWidgetsReferences()
	{
		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " inside setWidgetsReferences()");

		//playerListView 			= (ListView) findViewById(R.id.playerListView);
		mfTitleHomeScreen 		= (TextView) findViewById(R.id.mfTitleHomeScreen);
		lblPleaseSelectPlayer 	= (TextView) findViewById(R.id.lblPleaseSelectPlayer);
		textResult 				= (TextView) findViewById(R.id.textResult);
		btnTitleEdit 			= (TextView) findViewById(R.id.btnTitleEdit);
		lblAddPlayer 			= (Button)   findViewById(R.id.lblAddPlayer);
		playTitle    			= (Button)   findViewById(R.id.playTitle);
		btnTitleTop100          = (Button)   findViewById(R.id.btnTitleTop100);
		btnTitlePlayers         = (TextView) findViewById(R.id.btnTitlePlayers);
		linearLayout            = (LinearLayout) findViewById(R.id.userPlayerLayout);
		btnTitleBack            = (Button)   findViewById(R.id.btnTitleBack);
		myStudents              = (Button)   findViewById(R.id.btnMyStudents);

		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " outside setWidgetsReferences()");
	}

	/**
	 * this method set the widgets text values from the translation
	 */
	private void setWidgetsTextValue()
	{
		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " inside setWidgetsTextValue()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		mfTitleHomeScreen.setText(ITextIds.LEVEL+" "+transeletion.getTranselationTextByTextIdentifier(ITextIds.LBL_GARDE));
		lblPleaseSelectPlayer.setText(transeletion.getTranselationTextByTextIdentifier("lblSelectAPlayer"));
		textResult.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleResults"));
		btnTitleEdit.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleEdit"));
		lblAddPlayer.setText(transeletion.getTranselationTextByTextIdentifier("lblAddPlayer"));
		playTitle.setText(transeletion.getTranselationTextByTextIdentifier("playTitle")
				+ " " + transeletion.getTranselationTextByTextIdentifier("lblNow") + "!");
		btnTitleTop100.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleTop100"));
		btnTitlePlayers.setText(transeletion.getTranselationTextByTextIdentifier("btnTitlePlayers"));
		btnTitleBack.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleBack"));
		myStudents.setText(transeletion.getTranselationTextByTextIdentifier("resultTitleMyStudents"));
		transeletion.closeConnection();

		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " outside setWidgetsTextValue()");
	}

	/**
	 * this method get user player data from th database
	 */
	private void getUserPlayersData() 
	{
		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " inside getUserPlayersData()");

		UserPlayerOperation userPlayerObj = new UserPlayerOperation(this);
		userPlayerList = userPlayerObj.getUserPlayerData();

		if(LOGIN_USER_PLAYER_FLAG)
			Log.e(TAG, " outside getUserPlayersData()");
	}

	/**
	 * This method set the listener on wodgets
	 */
	private void setListenerOnWidgets()
	{
		lblAddPlayer.setOnClickListener(this);
		playTitle.setOnClickListener(this);
		btnTitleTop100.setOnClickListener(this);
		btnTitleBack.setOnClickListener(this);
		myStudents.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.lblAddPlayer:
			startActivity(new Intent(this,AddPlayer.class).putExtra("callingActivity", "TeacherPlayer"));
			break;
		case R.id.playTitle:
			if(callingActivity.equals("TeacherChallengeActivity"))
			{
				Intent intentMain = new Intent(this,TeacherChallengeActivity.class);
				startActivity(intentMain);
				finish();
			}
			else
			{
				startActivity(new Intent(this,MainActivity.class));
				finish();
			}
			break;
		case R.id.btnTitleBack:
			Intent intentMain = new Intent(this,MainActivity.class);
			startActivity(intentMain);
			break;
		case R.id.btnMyStudents:
			Intent intentStudents = new Intent(this,TeacherStudents.class);
			intentStudents.putExtra("callingActivity", "TeacherPlayer");
			startActivity(intentStudents);
			break;
		case R.id.btnTitleTop100 :
			SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
			if(!sheredPreference.getBoolean(IS_LOGIN, false))
			{
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustLoginOrRegisterToViewAndParticipate"));
				transeletion.closeConnection();	
			}
			else
			{
				startActivity(new Intent(this,Top100Activity.class));
			}
			break;
		}
	}

	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) 
	{
		if(keyCode == KeyEvent.KEYCODE_BACK)
		{
			if(callingActivity.equals("TeacherChallengeActivity"))
			{
				Intent intentMain = new Intent(this,TeacherChallengeActivity.class);
				startActivity(intentMain);
				finish();
			}
			else
			{
				Intent intentMain = new Intent(this,MainActivity.class);
				startActivity(intentMain);
				finish();
			}
			return false;
		}
		return super.onKeyDown(keyCode, event);
	}
}

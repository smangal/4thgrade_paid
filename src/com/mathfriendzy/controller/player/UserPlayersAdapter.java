package com.mathfriendzy.controller.player;

import static com.mathfriendzy.utils.ICommonUtils.IS_CHECKED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_ID;

import java.util.ArrayList;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.forthgradepaid.R;
import com.mathfriendzy.model.registration.UserPlayerDto;

/**
 * this adapter shows the user player on the user player Activity
 * @author Yashwant Singh
 *
 */
public class UserPlayersAdapter extends BaseAdapter
{
	private LayoutInflater mInflater 		   = null;
	private Context context 				   = null;
	private ArrayList<UserPlayerDto> playerList= null;
	private int resource 					   = 0;
	private ViewHolder vholder 				   = null;
	
	private SharedPreferences sheredPreference 	= null;
	private ImageView imgCheckHoderImage 		=  null;
	ArrayList<ImageView> imgChekedList			= null;
	private boolean isChecked  					= false;
	
	public UserPlayersAdapter(Context context,  int resource , ArrayList<UserPlayerDto> playerList)
	{
		this.resource 	= resource;
		mInflater 		= LayoutInflater.from(context);
		this.context 	= context;
		this.playerList = playerList;
		
		sheredPreference = context.getSharedPreferences(IS_CHECKED_PREFF, 0);
		imgChekedList = new ArrayList<ImageView>();
	}
	
	@Override
	public int getCount() 
	{
		return playerList.size();
	}

	@Override
	public Object getItem(int arg0) 
	{
		return null;
	}

	@Override
	public long getItemId(int arg0) 
	{
		return 0;
	}

	@Override
	public View getView(final int index, View view, ViewGroup viewGroup) 
	{
		if(view == null)
		{
			vholder 			   = new ViewHolder();
			view 				   = mInflater.inflate(resource, null);
			vholder.txtPlayeName   = (TextView) view.findViewById(R.id.txtPlayerName);
			vholder.btnPlareEdit   = (Button)   view.findViewById(R.id.btnEditPlayer);
			vholder.btnPlayerResult= (Button)   view.findViewById(R.id.btnPlayerResult);
			vholder.imgChecked     = (ImageView)view.findViewById(R.id.listPlayerCheckbox);
			view.setTag(vholder);			
		}
		else
		{
			vholder = (ViewHolder) view.getTag();
		}
		vholder.txtPlayeName.setText(playerList.get(index).getFirstname() + " " + playerList.get(index).getLastname());
		
		imgChekedList.add(vholder.imgChecked);
		
		if(sheredPreference.getString(PLAYER_ID, "").equals(playerList.get(index).getPlayerid()))
		{				
			vholder.imgChecked.setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
			imgCheckHoderImage = vholder.imgChecked;
		}
		else
		{
			vholder.imgChecked.setBackgroundResource(R.drawable.mf_check_box_ipad);
		}
		
		vholder.btnPlareEdit.setOnClickListener(new OnClickListener() 
		{			
			@Override
			public void onClick(View v) 
			{	
				Intent intent = new Intent(context,EditRegisteredUserPlayer.class);
				intent.putExtra("playerId", playerList.get(index).getPlayerid());
				intent.putExtra("imageName" , playerList.get(index).getImageName());
				intent.putExtra("userId", playerList.get(index).getParentUserId());
				Activity calling = ((Activity) context);
				intent.putExtra("callingActivity", calling.getClass().getSimpleName());
				context.startActivity(intent);
			}
		});
		
		vholder.btnPlayerResult.setOnClickListener(new OnClickListener() 
		{
			
			@Override
			public void onClick(View v) 
			{
				Log.e("", "Player result");
			}
		});
		
		vholder.imgChecked.setOnClickListener(new OnClickListener() 
		{			
			@Override
			public void onClick(View v) 
			{				
				for( int  i = 0 ; i < imgChekedList.size() ; i ++ )
				{
					if(v == imgChekedList.get(i))
					{
						SharedPreferences.Editor editor = sheredPreference.edit();
						
						if(imgCheckHoderImage != null)
						{
							imgCheckHoderImage.setBackgroundResource(R.drawable.mf_check_box_ipad);
							Log.e("", "inside if");
						}
						
						if(!isChecked)
						{								
							imgChekedList.get(i).setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
							editor.putString(PLAYER_ID,playerList.get(index).getPlayerid());
							imgCheckHoderImage = imgChekedList.get(i);
							isChecked = !isChecked;
						}
						else
						{
							
							if(imgCheckHoderImage == imgChekedList.get(i))
							{
								//Log.e(TAG, "inside if");
								imgChekedList.get(i).setBackgroundResource(R.drawable.mf_check_box_ipad);
								editor.clear();
								isChecked = !isChecked;
							}
							else
							{
								imgCheckHoderImage.setBackgroundResource(R.drawable.mf_check_box_ipad);
								imgCheckHoderImage = imgChekedList.get(i);
								imgChekedList.get(i).setBackgroundResource(R.drawable.mf_checked_check_box_ipad);
								editor.putString(PLAYER_ID,playerList.get(index).getPlayerid());
							}
						}
						editor.commit();
					}
				}
			//}
			}
		});
		
		return view;
	}

	
	/**
	 * static class for view which hole the static objects of views
	 * @author Yashwant Singh
	 *
	 */
	static class ViewHolder
	{
		TextView  txtPlayeName ;
		Button    btnPlareEdit;
		Button    btnPlayerResult;
		ImageView imgChecked;
	}
}

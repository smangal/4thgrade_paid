package com.mathfriendzy.controller.player;


import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.forthgradepaid.R;
import com.mathfriendzy.controller.base.AdBaseActivity;
import com.mathfriendzy.model.country.Country;
import com.mathfriendzy.model.grade.Grade;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.schools.SchoolDTO;
import com.mathfriendzy.model.schools.TeacherDTO;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DialogGenerator;

/**
 * @Description This class is common for all player 
 * @author Yashwant Singh
 *
 */
public class BasePlayer extends AdBaseActivity
{
	protected EditText edtFirstName = null;
	protected EditText edtLastName  = null;
	protected EditText edtUserName  = null;
	protected EditText edtCity      = null;
	protected EditText edtZipCode   = null;
	protected Spinner spinnerCountry= null;
	protected Spinner spinnerState  = null;
	protected ImageView imgAvtar	= null;
	protected Button btnTitleDelete = null;
	protected TextView pickerTitleSchool  	= null;
	protected Spinner spinnerSchool 	  	= null;
	protected TextView lblRegTeacher 		= null;
	protected Spinner spinnerTeacher		= null;

	protected EditText edtSchool            = null;//changes
	protected EditText edtTeacher           = null;//changes

	protected TextView mfTitleHomeScreen  = null;
	protected TextView lblEditPlayerTitle = null;
	protected Button   lblFbConnect 	  = null;
	protected TextView lblFirstName 	  = null;
	protected TextView lblLastName 		  = null;
	protected TextView lblUserName 		  = null;
	protected TextView lblRegCountry 	  = null;
	protected TextView lblRegState		  = null;
	protected TextView lblRegCity 		  = null;
	protected TextView lblRegZip 		  = null;
	protected TextView lblChooseAnAvatar  = null;
	protected RelativeLayout avtarLayout  = null;
	protected Spinner  spinnerGrade  		= null;
	protected TextView lblAddPlayerGrade 	= null;

	public static Bitmap profileImageBitmap       = null;
	public static String imageName                   = null;

	protected ArrayAdapter<String> schoolAdapter	  = null;
	protected ArrayList<String>    schoolNameList	  = null;
	protected ArrayList<SchoolDTO> schoolList		  = null;
	protected ArrayList<String>    schoolIdList	  	  = null;
	protected static ArrayList<TeacherDTO> teacherList= null;
	protected ArrayList<String> gradeList			  = null;
	protected ArrayList<String>     countryNameList   = null;

	protected String countryIso					  	  = null;
	protected String stateCodeName				      = null;

	protected final String SCHOOL_TEXT 		= "Cannot find my school";
	protected final String SCHOOL_ID 		= "";
	protected final String TEACHAER_TEXT  	= "Cannot find my teacher";

	public static String schoolId			= null;
	public static String teacherId           = null;

	public static boolean IS_IMAGE_FROM_CHOOSE_AVTAR	= false;
	public static boolean IS_INFO_FROM_FACEBOOK	 		= false;
	public static boolean IS_IMAGE_FROM_FACEBOOK	 	= false;

	ProgressDialog progressDialog = null;



	/**
	 * @Description getCountries from database and set it to the country adapter
	 * @param context
	 * @param countryName
	 */
	protected void getCountries(Context context,String countryName)
	{
		Country countryObj = new Country();
		countryNameList = countryObj.getCountryName(this);
		this.setCountryAdapter(context, countryName);
	}

	/**
	 * @description Set country adapter
	 * @param context
	 * @param countryName
	 */
	protected void setCountryAdapter(Context context,String countryName)
	{
		if(countryNameList != null)
		{
			ArrayAdapter<String> countryAdapter = new ArrayAdapter<String>(context, R.layout.spinner_country_item,
					countryNameList);
			countryAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
			spinnerCountry.setAdapter(countryAdapter);
			spinnerCountry.setSelection(countryAdapter.getPosition(countryName));
		}
	}

	/** 
	 * @Descritpion getGradeData from database 
	 * @param context
	 * @param gradeValue
	 */
	protected void getGrade(Context context,String gradeValue)
	{			
		Grade gradeObj = new Grade();
		gradeList = gradeObj.getGradeList(this);
		this.setGradeAdapter(context,gradeValue);
	}

	/**
	 * @Description set Grade data to adapter
	 * @param context
	 * @param gradeValue
	 */
	protected void setGradeAdapter(Context context,String gradeValue)
	{				
		ArrayAdapter<String> gradeAdapter = new ArrayAdapter<String>(context, android.R.layout.simple_list_item_1,gradeList);
		gradeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		spinnerGrade.setAdapter(gradeAdapter);
		spinnerGrade.setSelection(gradeAdapter.getPosition(gradeValue));
	}


	/**
	 * This asyncTask set image from facebook url to the imageView 
	 * @author Yashwant Singh
	 *
	 */
	class FacebookImageLoaderTask extends AsyncTask<Void, Void, Void>
	{
		private String strUrl = null;
		public FacebookImageLoaderTask(String strUrl)
		{
			this.strUrl = strUrl;
		}

		@Override
		protected Void doInBackground(Void... params) 
		{				
			URL img_value;
			try 
			{
				img_value = new URL(strUrl);
				profileImageBitmap = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());				
			} 
			catch (MalformedURLException e) 
			{			
				e.printStackTrace();
				Log.e("EditActivity", "Problem in setting image" + e);
			}
			catch(Exception ee)
			{
				Log.e("EditActivity", "Problem in setting image" + ee);
			}

			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			imgAvtar.setBackgroundResource(0);
			imgAvtar.setImageBitmap(profileImageBitmap);
			super.onPostExecute(result);
		}
	}

	/**
	 * This method set text from facebook and also image from chhose avtar
	 */
	protected void setTextFromFacebook() 
	{		
		if(IS_IMAGE_FROM_FACEBOOK)
		{
			SharedPreferences sheredPreference = getSharedPreferences("myPreff", 0);
			if(sheredPreference.getString("firstName", null) != null)
				edtFirstName.setText(sheredPreference.getString("firstName", null));
			else
				edtFirstName.setText("");
			if(sheredPreference.getString("lastName", null) != null)
				edtLastName.setText(sheredPreference.getString("lastName", null));
			else
				edtLastName.setText("");
			if(sheredPreference.getString("userName", null) != null)
				edtUserName.setText(sheredPreference.getString("userName", null));
			else
				edtUserName.setText("");

			IS_IMAGE_FROM_FACEBOOK = false;
			if(sheredPreference.getString("imgUrl", null) != null)
			{
				//changes for Internet connection
				if(CommonUtils.isInternetConnectionAvailable(this))
				{
					new FacebookImageLoaderTask(sheredPreference.getString("imgUrl", null)).execute(null,null,null);
				}
				else
				{
					DialogGenerator dg = new DialogGenerator(this);
					Translation transeletion = new Translation(this);
					transeletion.openConnection();
					dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
					transeletion.closeConnection();
				}
			}
		}

		if(IS_IMAGE_FROM_CHOOSE_AVTAR)
		{
			imgAvtar.setImageBitmap(profileImageBitmap);
			IS_IMAGE_FROM_CHOOSE_AVTAR = false;
		}
	}
}

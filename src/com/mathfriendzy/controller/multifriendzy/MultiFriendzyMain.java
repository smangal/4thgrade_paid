package com.mathfriendzy.controller.multifriendzy;

import static com.mathfriendzy.utils.ICommonUtils.FACEBOOK_HOST_NAME;
import static com.mathfriendzy.utils.ICommonUtils.IS_LOGIN;
import static com.mathfriendzy.utils.ICommonUtils.LOGIN_SHARED_PREFF;
import static com.mathfriendzy.utils.ICommonUtils.MULTI_FRIENDZY_MAIN_FLAG;
import static com.mathfriendzy.utils.ICommonUtils.PLAYER_INFO;

import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import android.annotation.SuppressLint;
import com.mathfriendzy.controller.base.AdBaseActivity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.forthgradepaid.R;
import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.controller.top100.Top100Activity;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.model.chooseAvtar.ChooseAvtarOpration;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.model.learningcenter.LearningCenterimpl;
import com.mathfriendzy.model.learningcenter.PlayerTotalPointsObj;
import com.mathfriendzy.model.multifriendzy.MultiFriendzyServerOperation;
import com.mathfriendzy.model.multifriendzy.MultiFriendzysFromServerDTO;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.DateTimeOperation;
import com.mathfriendzy.utils.DialogGenerator;

/**
 * This is the main activity of the multifriendzy
 * @author Yashwant Singh
 *
 */
public class MultiFriendzyMain extends AdBaseActivity implements OnClickListener
{
	private TextView labelTop 						= null;
	private Button   btnTop100 						= null;
	private TextView txtPlayerName  				= null;
	private TextView txtPlayerAddress				= null;
	private TextView txtPlayerPoints 				= null;		
	private ImageView imgPlayer     				= null;
	private Button  btnStartNewFriendzys            = null;
	private TextView txtYourFriendzys               = null;
	private Button  btnYourTurn                     = null;
	private Button  btnThierTurn                    = null;
	private Button  btnHistory                      = null;
 
	private RelativeLayout yourTurnLayout           = null;
	private RelativeLayout theirTurnLayout          = null;
	private RelativeLayout historyLayout            = null;

	private TextView txtPleaseTapForYoutTurn        = null;
	private TextView txtPleaseTapForTheirTurn       = null;
	private TextView txtPleaseTapForHistory         = null; 

	private final String TAG = this.getClass().getSimpleName();
	private int offset = 0;

	private Bitmap profileImageBitmap = null;

	private ImageView imgOpponentPlayer 	= null;
	private TextView  txtOpponentName   	= null;
	private TextView  txtRound 				= null;
	private TextView  txtPlayedDayAgo 		= null;


	ArrayList<MultiFriendzysFromServerDTO> yourTurnList 	= null;
	ArrayList<MultiFriendzysFromServerDTO> theirTurnList 	= null;
	ArrayList<MultiFriendzysFromServerDTO> historyList 		= null;

	private ArrayList<String> friendzyIdList = null;

	public static boolean isNewFriendzyStart = false;
	public static boolean isClickOnYourTurn  = false;

	//contains the translate text value from translation table
	private String theirTurnText = null;
	private String yourTurnText  = null;
	private String historyText   = null; 

	//for date diff
	private SimpleDateFormat df = null;
	private int diffInDays 		= 0 ;
	private int diffInHours     = 0;
	private int diffInMinuts    = 0;
	private int dayAgo 			= 0;

	String userId 	= null;
	String playerId = null;


	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_multi_friendzy_main);

		DisplayMetrics metrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(metrics);  

		if(metrics.heightPixels >= CommonUtils.TAB_HEIGHT && metrics.widthPixels <= CommonUtils.TAB_WIDTH
				&& metrics.densityDpi <= CommonUtils.TAB_DENISITY)	{

			setContentView(R.layout.activity_multi_friendzy_main_tab_low_denisity);
		}

		if(MULTI_FRIENDZY_MAIN_FLAG)
			Log.e(TAG, "inside onCreate()");

		friendzyIdList = new ArrayList<String>();

		this.setWidgetsReferences();
		this.setTextFromTranslation();
		this.setPlayerData();
		this.setListenerOnWidgets();

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);
		userId = sharedPreffPlayerInfo.getString("userId", "") ; 
		playerId = sharedPreffPlayerInfo.getString("playerId", "");

		if((!userId.equals("0") && !playerId.equals("0")))
		{
			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				/*String userId = "259";
				String playerId = "613";*/

				new GetFriendzysForPlayer(userId , playerId , offset).execute(null,null,null);
			}
			else
			{
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateInternetWarningDialog(transeletion.getTranselationTextByTextIdentifier(
						"alertMsgYouAreNotConnectedToTheInternet"),"Ok");
				transeletion.closeConnection();
			}
		}

		if(MULTI_FRIENDZY_MAIN_FLAG)
			Log.e(TAG, "outside onCreate()");
	}



	/**
	 * This method set the widgets references from the layout reference 
	 */
	private void setWidgetsReferences() 
	{
		if(MULTI_FRIENDZY_MAIN_FLAG)
			Log.e(TAG, "inside setWidgetsReferences()");

		labelTop 			= (TextView) 	findViewById(R.id.labelTop);
		btnTop100 			= (Button) 		findViewById(R.id.btnTop100);
		txtPlayerName 		= (TextView) 	findViewById(R.id.txtPlayerName);
		txtPlayerAddress 	= (TextView) 	findViewById(R.id.txtPlayerAddress);
		txtPlayerPoints 	= (TextView) 	findViewById(R.id.txtPlayerPoints);
		imgPlayer 			= (ImageView) 	findViewById(R.id.imgSmiley);
		btnStartNewFriendzys= (Button) 		findViewById(R.id.btnStartNewFriendzys);
		txtYourFriendzys 	= (TextView) 	findViewById(R.id.txtYourFriendzys);

		btnYourTurn 		= (Button) findViewById(R.id.btnYourTurn);
		btnThierTurn 		= (Button) findViewById(R.id.btnThierTurn);
		btnHistory 			= (Button) findViewById(R.id.btnHistory);

		yourTurnLayout 		= (RelativeLayout) findViewById(R.id.yourTurnLayout);
		theirTurnLayout 	= (RelativeLayout) findViewById(R.id.theirTurnLayout);
		historyLayout 		= (RelativeLayout) findViewById(R.id.historyLayout);

		txtPleaseTapForYoutTurn 	= (TextView) findViewById(R.id.txtPleaseTapForYoutTurn);
		txtPleaseTapForTheirTurn 	= (TextView) findViewById(R.id.txtPleaseTapForTheirTurn);
		txtPleaseTapForHistory 		= (TextView) findViewById(R.id.txtPleaseTapForHistory);

		if(MULTI_FRIENDZY_MAIN_FLAG)
			Log.e(TAG, "outside setWidgetsReferences()");
	}

	/**
	 * This method set the textvalue from translation
	 */
	private void setTextFromTranslation() 
	{
		if(MULTI_FRIENDZY_MAIN_FLAG)
			Log.e(TAG, "inside setTextFromTranslation()");

		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		labelTop.setText(transeletion.getTranselationTextByTextIdentifier("lblBestOf5Friendzy"));
		btnTop100.setText(transeletion.getTranselationTextByTextIdentifier("lblRateMe"));

		txtPlayerPoints.setText("0"
				+ " " + transeletion.getTranselationTextByTextIdentifier("btnTitlePoints"));

		btnStartNewFriendzys.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleStartNewFriendzy"));
		txtYourFriendzys.setText(transeletion.getTranselationTextByTextIdentifier("lblYourFriendzys"));

		theirTurnText = transeletion.getTranselationTextByTextIdentifier("lblTheirTurn");
		yourTurnText  = transeletion.getTranselationTextByTextIdentifier("lblYourTurn");
		historyText   = transeletion.getTranselationTextByTextIdentifier("lblHistory");

		btnYourTurn.setText(yourTurnText);
		btnThierTurn.setText(theirTurnText);
		btnHistory.setText(historyText);

		txtPleaseTapForYoutTurn.setText(transeletion.getTranselationTextByTextIdentifier("lblPleaseClickStartNewFriendzy"));
		txtPleaseTapForTheirTurn.setText(transeletion.getTranselationTextByTextIdentifier("lblPleaseClickStartNewFriendzy"));
		txtPleaseTapForHistory.setText(transeletion.getTranselationTextByTextIdentifier("lblPleaseClickStartNewFriendzy"));

		transeletion.closeConnection();

		if(MULTI_FRIENDZY_MAIN_FLAG)
			Log.e(TAG, "outside setTextFromTranslation()");
	}


	/**
	 * This method set the player detail
	 */
	private void setPlayerData() 
	{
		if(MULTI_FRIENDZY_MAIN_FLAG)
			Log.e(TAG, "inside setPlayerData()");

		SharedPreferences sharedPreffPlayerInfo = getSharedPreferences(PLAYER_INFO, 0);

		PlayerTotalPointsObj playerObj = null;
		LearningCenterimpl learningCenterimpl = new LearningCenterimpl(this);
		learningCenterimpl.openConn();

		if(sharedPreffPlayerInfo.getString("playerId", "0").equals(""))
		{
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints("0");
		}
		else
		{
			playerObj = learningCenterimpl.getDataFromPlayerTotalPoints
					(sharedPreffPlayerInfo.getString("playerId", "0"));
		}
		learningCenterimpl.closeConn();			

		txtPlayerName.setText(sharedPreffPlayerInfo.getString("playerName", ""));

		if(sharedPreffPlayerInfo.getString("state", "").equals(""))
		{
			txtPlayerAddress.setText(sharedPreffPlayerInfo.getString("city", ""));
		}
		else
		{
			txtPlayerAddress.setText(sharedPreffPlayerInfo.getString("city", "")
					+ ", " + sharedPreffPlayerInfo.getString("state", ""));
		}

		Translation transeletion1 = new Translation(this);
		transeletion1.openConnection();

		DateTimeOperation numberformat = new DateTimeOperation();
		txtPlayerPoints.setText(numberformat.setNumberString(playerObj.getTotalPoints() + "")
				+ " " + transeletion1.getTranselationTextByTextIdentifier("btnTitlePoints"));

		transeletion1.closeConnection();

		String imageName = sharedPreffPlayerInfo.getString("imageName",null);
		try
		{
			Long.parseLong(imageName);
			//changes for Internet Connection
			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				String strUrl = FACEBOOK_HOST_NAME + imageName + "/picture?type=large";
				new FacebookImageLoaderTask(strUrl , imgPlayer).execute(null,null,null);
			}
			else
			{
				DialogGenerator dg = new DialogGenerator(this);
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				dg.generateWarningDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouAreNotConnectedToTheInternet"));
				transeletion.closeConnection();
			}
		}
		catch(NumberFormatException ee)
		{
			ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
			chooseAvtarObj.openConn(this);
			if(chooseAvtarObj.getAvtarImageByName(imageName) != null)
			{
				profileImageBitmap = CommonUtils.getBitmapFromByte(
						chooseAvtarObj.getAvtarImageByName(imageName), this);
				imgPlayer.setImageBitmap(profileImageBitmap);
			}
			chooseAvtarObj.closeConn();
		}

		if(MULTI_FRIENDZY_MAIN_FLAG)
			Log.e(TAG, "outside setPlayerData()");
	}

	/**
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets() 
	{
		if(MULTI_FRIENDZY_MAIN_FLAG)
			Log.e(TAG, "inside setListenerOnWidgets()");

		btnStartNewFriendzys.setOnClickListener(this);
		btnTop100.setOnClickListener(this);

		if(MULTI_FRIENDZY_MAIN_FLAG)
			Log.e(TAG, "outside setListenerOnWidgets()");
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{
		case R.id.btnStartNewFriendzys:
			if((!userId.equals("0") && !playerId.equals("0")))
			{
				isNewFriendzyStart = true;
				isClickOnYourTurn  = false;
				startActivity(new Intent(this, StartNewFriendzyActivity.class));
			}
			else
			{
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgPleaseRegisterToSavePlayersInfo"));
				transeletion.closeConnection();	
			}
			break;
		case R.id.btnTop100 :
			/*SharedPreferences sheredPreference = getSharedPreferences(LOGIN_SHARED_PREFF, 0);
			if(!sheredPreference.getBoolean(IS_LOGIN, false))
			{
				Translation transeletion = new Translation(this);
				transeletion.openConnection();
				DialogGenerator dg = new DialogGenerator(this);
				dg.generateRegisterOrLogInDialog(transeletion.getTranselationTextByTextIdentifier("alertMsgYouMustLoginOrRegisterToViewAndParticipate"));
				transeletion.closeConnection();	
			}
			else
			{
				startActivity(new Intent(this,Top100Activity.class));
			}*/
			MathFriendzyHelper.rateUs(this);
			break;
		}
	}


	/**
	 * This method create dynamic layout for your turn 
	 */
	private void createDyanamicLayoutForYourTurn(final ArrayList<MultiFriendzysFromServerDTO> yourTurnList)
	{
		final LinearLayout layout = new LinearLayout(this);
		layout.setOrientation(LinearLayout.VERTICAL);

		RelativeLayout child = new RelativeLayout(this);
		Translation transeletion = new Translation(this);
		transeletion.openConnection();

		for( int i = 0 ; i < yourTurnList.size() ; i ++ )
		{
			LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
			child = (RelativeLayout) inflater.inflate(R.layout.layout_opponent_data, null);
			layout.addView(child);

			imgOpponentPlayer = (ImageView) child.findViewById(R.id.imgSmiley);
			txtOpponentName   = (TextView)  child.findViewById(R.id.txtPlayerName);
			txtRound          = (TextView)  child.findViewById(R.id.txtRound);
			txtPlayedDayAgo   = (TextView)  child.findViewById(R.id.txtPlayDayAgo);

			this.setImage(imgOpponentPlayer, yourTurnList.get(i).getOpponentData().getProfileImageNameId());
			txtOpponentName.setText(yourTurnList.get(i).getOpponentData().getfName() + " " 
					+ yourTurnList.get(i).getOpponentData().getlName());
			txtRound.setText(transeletion.getTranselationTextByTextIdentifier("lblRound") + " " 
					+ yourTurnList.get(i).getRoundList().size());

			dayAgo = this.getDiffInDay(yourTurnList.get(i).getStartingFriendzyDate());

			if(dayAgo > 0)
				txtPlayedDayAgo.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayed") + " " 
						+ dayAgo + " " + transeletion.getTranselationTextByTextIdentifier("lblDays") + " "
						+ transeletion.getTranselationTextByTextIdentifier("lblAgo"));
			else if(diffInHours > 0)
				txtPlayedDayAgo.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayed") + " " 
						+ diffInHours + " " + transeletion.getTranselationTextByTextIdentifier("lblHours") + " "
						+ transeletion.getTranselationTextByTextIdentifier("lblAgo"));
			else
				txtPlayedDayAgo.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayed") + " " 
						+ diffInMinuts + " " + transeletion.getTranselationTextByTextIdentifier("lblMinutes") + " "
						+ transeletion.getTranselationTextByTextIdentifier("lblAgo"));

			child.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{				
					for( int i = 0 ; i < yourTurnList.size() ; i ++ )
					{
						if(((RelativeLayout)v) == layout.getChildAt(i))
						{		
							MultiFriendzyRound.multiFriendzyServerDto 	= yourTurnList.get(i);
							MultiFriendzyRound.opponentImageId 			= yourTurnList.get(i).getOpponentData().getProfileImageNameId();
							MultiFriendzyRound.oppenentPlayerName 		= yourTurnList.get(i).getOpponentData().getfName()
									+ " " + yourTurnList.get(i).getOpponentData().getlName().charAt(0);
							MultiFriendzyRound.roundList = yourTurnList.get(i).getRoundList();

							MultiFriendzyRound.turn = yourTurnText;
							MultiFriendzyRound.type = yourTurnList.get(i).getType();
							MultiFriendzyRound.friendzyId = yourTurnList.get(i).getFriendzysId();

							isNewFriendzyStart = false;
							isClickOnYourTurn  = true;

							Intent intent = new Intent(MultiFriendzyMain.this , MultiFriendzyRound.class);
							//intent.putExtra("datafromServer", yourTurnList.get(i));
							startActivity(intent);
						}
					}
				}
			});
		}

		transeletion.closeConnection();
		yourTurnLayout.addView(layout);
	}

	/**
	 * This method create dynamic layout for their turn
	 * @param yourTurnList
	 */
	private void createDyanamicLayotForTheirTurn(final ArrayList<MultiFriendzysFromServerDTO> theirTurnList)
	{		
		final LinearLayout layout = new LinearLayout(this);
		layout.setOrientation(LinearLayout.VERTICAL);

		RelativeLayout child = new RelativeLayout(this);
		Translation transeletion = new Translation(this);
		transeletion.openConnection();

		for( int i = 0 ; i < theirTurnList.size() ; i ++ )
		{
			LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
			child = (RelativeLayout) inflater.inflate(R.layout.layout_opponent_data, null);
			layout.addView(child);

			imgOpponentPlayer = (ImageView) child.findViewById(R.id.imgSmiley);
			txtOpponentName   = (TextView)  child.findViewById(R.id.txtPlayerName);
			txtRound          = (TextView)  child.findViewById(R.id.txtRound);
			txtPlayedDayAgo   = (TextView)  child.findViewById(R.id.txtPlayDayAgo);

			this.setImage(imgOpponentPlayer, theirTurnList.get(i).getOpponentData().getProfileImageNameId());
			txtOpponentName.setText(theirTurnList.get(i).getOpponentData().getfName() + " " 
					+ theirTurnList.get(i).getOpponentData().getlName());
			txtRound.setText(transeletion.getTranselationTextByTextIdentifier("lblRound") + " " 
					+ theirTurnList.get(i).getRoundList().size());

			dayAgo = this.getDiffInDay(theirTurnList.get(i).getStartingFriendzyDate());

			if(dayAgo > 0)
				txtPlayedDayAgo.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayed") + " " 
						+ dayAgo + " " + transeletion.getTranselationTextByTextIdentifier("lblDays") + " "
						+ transeletion.getTranselationTextByTextIdentifier("lblAgo"));
			else if(diffInHours > 0)
				txtPlayedDayAgo.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayed") + " " 
						+ diffInHours + " " + transeletion.getTranselationTextByTextIdentifier("lblHours") + " "
						+ transeletion.getTranselationTextByTextIdentifier("lblAgo"));
			else
				txtPlayedDayAgo.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayed") + " " 
						+ diffInMinuts + " " + transeletion.getTranselationTextByTextIdentifier("lblMinutes") + " "
						+ transeletion.getTranselationTextByTextIdentifier("lblAgo"));

			/*txtPlayedDayAgo.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayed") + " " 
								+ dayAgo + " " + transeletion.getTranselationTextByTextIdentifier("lblDays") + " "
									+ transeletion.getTranselationTextByTextIdentifier("lblAgo"));*/

			child.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{	
					for( int i = 0 ; i < theirTurnList.size() ; i ++ )
					{
						if(((RelativeLayout)v) == layout.getChildAt(i))
						{				

							MultiFriendzyRound.multiFriendzyServerDto = theirTurnList.get(i);
							MultiFriendzyRound.opponentImageId 		  = theirTurnList.get(i).getOpponentData().getProfileImageNameId();
							MultiFriendzyRound.oppenentPlayerName 	  = theirTurnList.get(i).getOpponentData().getfName()
									+ " " + theirTurnList.get(i).getOpponentData().getlName().charAt(0);
							MultiFriendzyRound.roundList = theirTurnList.get(i).getRoundList();

							MultiFriendzyRound.turn = theirTurnText;
							MultiFriendzyRound.type = theirTurnList.get(i).getType();
							MultiFriendzyRound.friendzyId = theirTurnList.get(i).getFriendzysId();

							Intent intent = new Intent(MultiFriendzyMain.this , MultiFriendzyRound.class);
							//intent.putExtra("datafromServer", theirTurnList.get(i));
							startActivity(intent);
						}
					}
				}
			});
		}

		transeletion.closeConnection();
		theirTurnLayout.addView(layout);
	}

	/**
	 * This method return the diff in day 
	 * @param playData
	 * @return
	 */
	@SuppressLint("SimpleDateFormat")
	private int getDiffInDay(String playDate)
	{

		df = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");
		diffInDays = 0;

		try 
		{
			Date startDate = df.parse(playDate);
			Date currentData = new Date();
			if(playDate.equals("0000-00-00 00:00:00"))
			{
				startDate = currentData;
			}

			diffInDays 		= (int)( (currentData.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24) );
			diffInHours 	= (int)( (currentData.getTime() - startDate.getTime()) / (1000 * 60 * 60 ) ) - 24 * diffInDays;
			diffInMinuts 	= (int)( (currentData.getTime() - startDate.getTime()) / (1000  * 60 ) ) - (diffInHours + 24 * diffInDays) * 60;
		} 
		catch (ParseException e) 
		{
			e.printStackTrace();
		}
		if(diffInMinuts < 0)
		{
			diffInMinuts = 0;
		}

		return diffInDays;
	}


	/**
	 * This method create dynamic layout for history
	 * @param historyList
	 */
	private void createDyamiLayoutForHistory(final ArrayList<MultiFriendzysFromServerDTO> historyList)
	{
		final LinearLayout layout = new LinearLayout(this);
		layout.setOrientation(LinearLayout.VERTICAL);

		RelativeLayout child = new RelativeLayout(this);
		Translation transeletion = new Translation(this);
		transeletion.openConnection();

		for( int i = 0 ; i < historyList.size() ; i ++ )
		{
			LayoutInflater inflater = (LayoutInflater) getSystemService(this.LAYOUT_INFLATER_SERVICE);
			child = (RelativeLayout) inflater.inflate(R.layout.layout_opponent_data, null);
			layout.addView(child);

			imgOpponentPlayer = (ImageView) child.findViewById(R.id.imgSmiley);
			txtOpponentName   = (TextView)  child.findViewById(R.id.txtPlayerName);
			txtRound          = (TextView)  child.findViewById(R.id.txtRound);
			txtPlayedDayAgo   = (TextView)  child.findViewById(R.id.txtPlayDayAgo);

			this.setImage(imgOpponentPlayer, historyList.get(i).getOpponentData().getProfileImageNameId());
			txtOpponentName.setText(historyList.get(i).getOpponentData().getfName() + " " 
					+ historyList.get(i).getOpponentData().getlName());

			if((historyList.get(i).getWinner() + "").equals(playerId))
			{
				//MultiFriendzyWinnerScreen.isWinner = true;
				txtRound.setText(transeletion.getTranselationTextByTextIdentifier("lblYouWon") + "!");

			}
			else
			{
				//MultiFriendzyWinnerScreen.isWinner = false;
				txtRound.setText(transeletion.getTranselationTextByTextIdentifier("lblYouLost") + "!");
			}

			dayAgo = this.getDiffInDay(historyList.get(i).getStartingFriendzyDate());

			if(dayAgo > 0)
				txtPlayedDayAgo.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayed") + " " 
						+ dayAgo + " " + transeletion.getTranselationTextByTextIdentifier("lblDays") + " "
						+ transeletion.getTranselationTextByTextIdentifier("lblAgo"));
			else if(diffInHours > 0)
				txtPlayedDayAgo.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayed") + " " 
						+ diffInHours + " " + transeletion.getTranselationTextByTextIdentifier("lblHours") + " "
						+ transeletion.getTranselationTextByTextIdentifier("lblAgo"));
			else
				txtPlayedDayAgo.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayed") + " " 
						+ diffInMinuts + " " + transeletion.getTranselationTextByTextIdentifier("lblMinutes") + " "
						+ transeletion.getTranselationTextByTextIdentifier("lblAgo"));

			/*txtPlayedDayAgo.setText(transeletion.getTranselationTextByTextIdentifier("lblPlayed") + " " 
								+ dayAgo + " " + transeletion.getTranselationTextByTextIdentifier("lblDays") + " "
									+ transeletion.getTranselationTextByTextIdentifier("lblAgo"));*/

			child.setOnClickListener(new OnClickListener() 
			{
				@Override
				public void onClick(View v) 
				{		
					for( int i = 0 ; i < historyList.size() ; i ++ )
					{
						if(((RelativeLayout)v) == layout.getChildAt(i))
						{						
							if((historyList.get(i).getWinner() + "").equals(playerId))
							{
								MultiFriendzyWinnerScreen.isWinner = true;
							}
							else
							{
								MultiFriendzyWinnerScreen.isWinner = false;
							}
							MultiFriendzyRound.multiFriendzyServerDto 		= historyList.get(i);
							MultiFriendzyWinnerScreen.oppenentPlayerName 	= historyList.get(i).getOpponentData().getfName() + " " 
									+ historyList.get(i).getOpponentData().getlName().charAt(0);
							MultiFriendzyWinnerScreen.opponentImageId    = historyList.get(i).getOpponentData().getProfileImageNameId();
							MultiFriendzyWinnerScreen.roundList          = historyList.get(i).getRoundList();

							startActivity(new Intent(MultiFriendzyMain.this , MultiFriendzyWinnerScreen.class));
						}
					}
				}
			});
		}

		transeletion.closeConnection();
		historyLayout.addView(layout);
	}


	/**
	 * This method set the opponent image
	 * @param imgOpponentPlayer
	 * @param imageName
	 */
	private void setImage(ImageView imgOpponentPlayer , String imageName)
	{
		try
		{
			Long.parseLong(imageName);
			//changes for Internet Connection
			if(CommonUtils.isInternetConnectionAvailable(this))
			{
				String strUrl = FACEBOOK_HOST_NAME + imageName + "/picture?type=large";
				new FacebookImageLoaderTask(strUrl , imgOpponentPlayer).execute(null,null,null);
			}
			else
			{
				imgOpponentPlayer.setBackgroundResource(R.drawable.smiley);

			}
		}
		catch(NumberFormatException ee)
		{
			ChooseAvtarOpration chooseAvtarObj = new ChooseAvtarOpration();
			chooseAvtarObj.openConn(this);
			if(chooseAvtarObj.getAvtarImageByName(imageName) != null)
			{
				profileImageBitmap = CommonUtils.getBitmapFromByte(
						chooseAvtarObj.getAvtarImageByName(imageName), this);
				imgOpponentPlayer.setImageBitmap(profileImageBitmap);
			}
			chooseAvtarObj.closeConn();
		}
	}


	/**
	 * This asyncTask set image from facebook url to the Button 
	 * @author Yashwant Singh
	 *
	 */
	class FacebookImageLoaderTask extends AsyncTask<Void, Void, Void>
	{
		private String strUrl = null;
		private ImageView imgPlayer = null;
		public FacebookImageLoaderTask(String strUrl , ImageView imgPlayer)
		{
			this.strUrl = strUrl;
			this.imgPlayer = imgPlayer;
		}

		@Override
		protected Void doInBackground(Void... params) 
		{				
			URL img_value;
			try 
			{
				img_value = new URL(strUrl);
				profileImageBitmap = BitmapFactory.decodeStream(img_value.openConnection().getInputStream());
			} 
			catch (MalformedURLException e) 
			{			
				e.printStackTrace();
				Log.e("EditActivity", "Problem in setting image" + e);
			}
			catch(Exception ee)
			{
				Log.e("EditActivity", "Problem in setting image" + ee);
			}
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			imgPlayer.setImageBitmap(profileImageBitmap);
			imgPlayer.invalidate();
			super.onPostExecute(result);
		}
	}

	@Override
	public void onBackPressed() 
	{
		startActivity(new Intent(this , MainActivity.class));
		super.onBackPressed();
	}


	/**
	 * This aysnckTask get the friendzys for player
	 * @author Yashwant Singh
	 *
	 */
	class GetFriendzysForPlayer extends AsyncTask<Void, Void, Void>
	{		
		private String userId 	= null;
		private String playerId = null;
		private int offset = 0;

		private ProgressDialog pd = null;

		private ArrayList<MultiFriendzysFromServerDTO> mathFriedzysList = null;

		GetFriendzysForPlayer(String userId , String playerId , int offSet)
		{
			this.userId = userId;
			this.playerId = playerId;
			this.offset = offSet;
		}

		@Override
		protected void onPreExecute() 
		{		
			mathFriedzysList = new ArrayList<MultiFriendzysFromServerDTO>();

			pd = CommonUtils.getProgressDialog(MultiFriendzyMain.this);
			pd.show();

			super.onPreExecute();
		}

		@Override
		protected Void doInBackground(Void... params) 
		{		
			MultiFriendzyServerOperation multiFriendzyServerObj = new MultiFriendzyServerOperation();
			mathFriedzysList = multiFriendzyServerObj.getFriendzysForPlayer(userId, playerId, offset);
			return null;
		}

		@Override
		protected void onPostExecute(Void result) 
		{
			pd.cancel();

			/*if(mathFriedzysList.size() > 0)
			{
				for( int i = 0 ; i < mathFriedzysList.size() ; i ++ )
				{
					Log.e(TAG, "Country " + mathFriedzysList.get(i).getStartingFriendzyDate() + " \n ");
				}
			}*/

			if(mathFriedzysList.size() > 0)
			{
				yourTurnList = new ArrayList<MultiFriendzysFromServerDTO>();
				theirTurnList = new ArrayList<MultiFriendzysFromServerDTO>();
				historyList = new ArrayList<MultiFriendzysFromServerDTO>();

				for( int i = 0 ; i < mathFriedzysList.size() ; i ++ )
				{
					if(mathFriedzysList.get(i).getIsCompleted() == 1 && mathFriedzysList.get(i).getWinner() > 0)
					{
						historyList.add(mathFriedzysList.get(i));
					}
					else
					{
						if(mathFriedzysList.get(i).getType().equals("their friendzy"))
						{
							theirTurnList.add(mathFriedzysList.get(i));
						}
						else
						{
							yourTurnList.add(mathFriedzysList.get(i));
						}
					}
				}


				if(theirTurnList.size() > 0 )
				{
					txtPleaseTapForTheirTurn.setVisibility(TextView.GONE);
					createDyanamicLayotForTheirTurn(theirTurnList);
				}

				if(yourTurnList.size() > 0)
				{
					txtPleaseTapForYoutTurn.setVisibility(TextView.GONE);
					createDyanamicLayoutForYourTurn(yourTurnList);
				}

				if(historyList.size() > 0)
				{
					txtPleaseTapForHistory.setVisibility(TextView.GONE);
					createDyamiLayoutForHistory(historyList);
				}
			}

			super.onPostExecute(result);
		}
	}


}

package com.mathfriendzy.social.google;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.util.Log;
import android.util.Patterns;

import java.util.ArrayList;
import java.util.regex.Pattern;

/**
 * Created by root on 9/4/15.
 */
public class UserEmailFetcher {

   public static String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);

        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }

    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }

    public static CharSequence[] getAllLoginEmail(Context context){
        Pattern emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
        Account[] accounts = AccountManager.get(context).getAccounts();
        ArrayList<String> accountList = new ArrayList<String>();
        for(int i = 0 ; i < accounts.length ; i ++ ){
            Account account = accounts[i];
            if (emailPattern.matcher(account.name).matches()) {
                String possibleEmail = account.name;
                accountList.add(possibleEmail);
                //loginEmailList[i] = possibleEmail;
            }
        }

        CharSequence[] loginEmailList = new CharSequence[accountList.size()];
        for(int i = 0 ; i < accountList.size() ; i ++ ){
            loginEmailList[i] = accountList.get(i);
        }
        return loginEmailList;
    }
}

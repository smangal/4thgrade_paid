package com.mathfriendzy.utils;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Handler;

import com.mathfriendzy.controller.main.MainActivity;
import com.mathfriendzy.model.language.Language;
import com.mathfriendzy.model.language.translation.Translation;

/**
 * This AsyncTask execute at the starting of the project in which the translation , languages from the server loaded 
 * into the database
 * @author Shilpi Mangal
 *
 */
public class StartUpAsyncTask extends AsyncTask<Void, Void, Void> 
{	
	private static boolean isGetLanguageFromServer 	= false;
	private static boolean isTranselation 			= false;
	private ProgressDialog pd;

	//private final String TAG = this.getClass().getSimpleName();
	private Context context = null;

	public StartUpAsyncTask(Context context)
	{
		MainActivity.isAvtarLoaded = false;
		this.context = context;
	}

	@Override
	protected void onPreExecute()
	{
		pd  = CommonUtils.getProgressDialog(context);
		pd.setMessage("Please be patient. This could take several minutes.");
		pd.show();
		pd.setCancelable(false);
		super.onPreExecute();
	}

	@Override
	protected Void doInBackground(Void... params) 
	{
		SharedPreferences sheredPreference = context.getSharedPreferences("SHARED_FLAG", 0);	
		isGetLanguageFromServer = sheredPreference.getBoolean("isGetLanguageFromServer", false);
		isTranselation			= sheredPreference.getBoolean("isTranselation", false);

		SharedPreferences.Editor editor = sheredPreference.edit();

		/*
		 * use to return because no need to go inside
		 */
		if(isGetLanguageFromServer && isTranselation)
		{
			return null;
		}

		if(CommonUtils.isInternetConnectionAvailable(context))
		{
			if(!isGetLanguageFromServer)
			{
				editor.putBoolean("isGetLanguageFromServer", true);			
				//Log.e(TAG, "isGetLanguageFromServer");
				Language languageFromServer = new Language(context);
				languageFromServer.getLanguagesFromServer();
				languageFromServer.saveLanguages();

			}

			if(!isTranselation)
			{
				editor.putBoolean("isTranselation", true);
				editor.commit();
				//Log.e(TAG, "isTranselation");
				Translation traneselation = new Translation(context);
				traneselation.getTransalateTextFromServer(CommonUtils.LANGUAGE_CODE, CommonUtils.LANGUAGE_ID, CommonUtils.APP_ID);
				traneselation.saveTranslationText();

			}

		}

		//Log.e("", "inside doInBackGround()");
		return null;
	}

	@Override
	protected void onPostExecute(Void result) 
	{	
		if(pd != null)
			pd.cancel();
		//starting main Activity after completion of background task
		new Handler().postDelayed(new Runnable() {

			@Override
			public void run() 
			{
				Intent intent = new Intent(context,MainActivity.class);
				context.startActivity(intent);
				((Activity)context).finish();
			}
		}, 1000);

		super.onPostExecute(result);
	}
	
}

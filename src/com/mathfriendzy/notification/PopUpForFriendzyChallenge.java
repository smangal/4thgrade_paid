package com.mathfriendzy.notification;

import com.mathfriendzy.controller.base.AdBaseActivity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import com.forthgradepaid.R;
import com.mathfriendzy.controller.friendzy.FriendzyChallengeActivity;
import com.mathfriendzy.controller.friendzy.FriendzyNotificationPlayerActivity;
import com.mathfriendzy.controller.friendzy.StudentChallengeActivity;
import com.mathfriendzy.gcm.ProcessNotification;
import com.mathfriendzy.model.friendzy.FriendzyDTO;
import com.mathfriendzy.model.language.translation.Translation;
import com.mathfriendzy.utils.FriendzyUtils;

public class PopUpForFriendzyChallenge extends AdBaseActivity implements OnClickListener
{
	private Button btnOk 		= null;
	private Button btnCencel 	= null;
	private TextView txtMessage = null;
	private boolean isActive	= false;
	private FriendzyDTO dto		= null;
	public static boolean isDelete;

	@Override
	protected void onCreate(Bundle savedInstanceState) 
	{
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_pop_up_activity_for_notification);

		this.setWidgetsReferences();
		this.setListenerOnWidgets();
		this.setTextFromtranselation();
	}

	/**
	 * This method set widgets references
	 */
	private void setWidgetsReferences()
	{
		String grade = null;

		btnOk 		= (Button) findViewById(R.id.btnOk);
		btnCencel 	= (Button) findViewById(R.id.btnCencel);
		txtMessage  = (TextView) findViewById(R.id.txtMessageFromNotification);

		String msg;
		dto = ProcessNotification.dto;
		if(!isDelete)
		{		
			if(dto.getGrade() != null){
				try
				{
					grade = setGrade(Integer.parseInt(dto.getGrade()));
				}
				catch(Exception e)
				{
					grade = dto.getGrade();
				}
			}
			if(dto.getStatus() != null && dto.getStatus().equals("1"))
			{
				isActive = true;
				msg	= dto.getTeacherName()+", a "+grade+" grade teacher from "+dto.getSchoolName()
						+" has created challenge for you: "+dto.getClassName()+
						".\n\n Would you like to accept this challenge?";
			}
			else
			{
				isActive = false;
				msg = ""+dto.getClassName()+" Friendzy Challenge, created by, "+dto.getTeacherName()+", is now completed. \n" +
						"Would you like to see the final leaderboard for this challenge?";
			}

		}//Editing for deletePlayer Notification
		else
		{
			FriendzyUtils.deActivatePlayer(dto.getChallengerId(), dto.getPid());
			btnOk.setVisibility(View.GONE);	
			msg = dto.getRewards()+"\n\n"+dto.getClassName()+", "+dto.getTeacherName()
					+"\n"+dto.getStartDate()+" to "+dto.getEndDate()+".";
		}//End if else


		txtMessage.setText(msg);
	}

	/**
	 * This method set text from translation
	 */
	private void setTextFromtranselation()
	{
		Translation transeletion = new Translation(this);
		transeletion.openConnection();
		btnOk.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));
		btnCencel.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleCancel"));
		if(isDelete)
			btnCencel.setText(transeletion.getTranselationTextByTextIdentifier("btnTitleOK"));

		transeletion.closeConnection();
	}

	/**
	 * This method set listener on widgets
	 */
	private void setListenerOnWidgets()
	{
		btnOk.setOnClickListener(this);

		btnCencel.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) 
	{
		switch(v.getId())
		{		
		case R.id.btnOk :
			Intent intent = null;
			if(isActive)
			{
				intent = new Intent(this,FriendzyNotificationPlayerActivity.class);
			}
			else
			{
				StudentChallengeActivity.friendzyObj = dto;
				intent = new Intent(this,FriendzyChallengeActivity.class);
				intent.putExtra("challengerId", dto.getChallengerId());
			}

			startActivity(intent);
			finish();
			break;
		case R.id.btnCencel : 
			finish();
			break;
		}
	}

	/**
	 * use to set standard with grade
	 * @param grade
	 */
	private String setGrade(int grade)
	{
		String alias;
		switch(grade)
		{
		case 1:
			alias = "1st";
			break;
		case 2:
			alias = "2nd";
			break;
		case 3:
			alias = "3rd";
			break;
		case 13:
			alias = "adult";
		default :
			alias = grade+"th";
		}	

		return alias;

	}//END setGrade method
}

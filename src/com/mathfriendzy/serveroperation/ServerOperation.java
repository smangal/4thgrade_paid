package com.mathfriendzy.serveroperation;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import android.util.Log;

import com.mathfriendzy.controller.base.MyApplication;
import com.mathfriendzy.controller.resources.GetKhanVideoLinkParam;
import com.mathfriendzy.controller.resources.GetResourceCategoriesParam;
import com.mathfriendzy.controller.resources.ResourceParam;
import com.mathfriendzy.helper.MathFriendzyHelper;
import com.mathfriendzy.newinappclasses.GetAppUnlockStatusParam;
import com.mathfriendzy.newinappclasses.GetResourceVideoInAppStatusParam;
import com.mathfriendzy.newinappclasses.UpdateUserCoinsParam;
import com.mathfriendzy.utils.CommonUtils;
import com.mathfriendzy.utils.ICommonUtils;



/**
 * This is the class where all the server opeation will be done
 * @author Yashwant Singh
 *
 */
public class ServerOperation {

	private final static String TAG = "ServerOperation";

	/** This method read the data from url and return it into string form */
	public static String readFromURL(String strURL) {

		StringBuffer finalString = new StringBuffer();
		try {
			URL url = new URL(strURL);

			BufferedReader in = new BufferedReader(new
					InputStreamReader(url.openStream()), 8*1024);

			String str = "";
			while ((str = in.readLine()) != null){
				finalString.append(str);
			}
			in.close();
		}
		catch (Exception e){
			Log.e(TAG, "Error while reading from url " + e.toString());
			return null;
		}
		return finalString.toString();
	}

	/**
	 * This method read the data from url and return it into string form
	 * @param nameValuePairs
	 * @return
	 */
	public static String readFromURL(ArrayList<NameValuePair> nameValuePairs , String url){

		if(CommonUtils.LOG_ON){
			Log.e(TAG , " url " + url);
			for(int i = 0 ; i < nameValuePairs.size() ; i ++ ){
				Log.e("requested param " , nameValuePairs.get(i) + "");
			}
		}

		InputStream inputStraem		= null;
		StringBuilder buffer 		= new StringBuilder("");

		try {
			HttpClient httpclient = new DefaultHttpClient();
			HttpPost httppost = new HttpPost(url);
			httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));
			HttpResponse response = httpclient.execute(httppost);
			HttpEntity entity = response.getEntity();
			inputStraem = entity.getContent();

			BufferedReader reader = new BufferedReader(new InputStreamReader
					(inputStraem,"iso-8859-1"),8);
			buffer = new StringBuilder();

			String line = null;
			while ((line = reader.readLine()) != null){
				buffer.append(line);
			}
			inputStraem.close();
		}
		catch (Exception e){
			Log.e(TAG, "Error while reading from url " + e.toString());
			return null;
		}
		return buffer.toString();
	}

	/**
	 * Create the string url for the post request
	 * @param nameValuePairs
	 * @param url
	 * @return
	 */
	public static String getUrl(ArrayList<NameValuePair> nameValuePairs , String url){
		StringBuilder strBuilder = new StringBuilder(url);
		for(int i = 0 ; i < nameValuePairs.size() ; i ++ ){
			if(i == 0)
				strBuilder.append(nameValuePairs.get(i));
			else
				//strBuilder.append("%26" + nameValuePairs.get(i));
				strBuilder.append("&" + nameValuePairs.get(i));
		}
		//return url + URLEncoder.encode(strBuilder.toString());
		return strBuilder.toString();
	}

	/**
	 * When we are sending a post request then we need a request url
	 * So We get url by using the request code
	 * @param requestCode
	 * @return
	 */
	public static String getUrl(int requestCode){
		if(requestCode == ServerOperationUtil.GET_RESOURCE_VIDEO_IN_APP_STATUS_REQUEST)
			return ICommonUtils.COMPLETE_URL_FROM_MATH;
		if(requestCode == ServerOperationUtil.SAVE_RESOURCE_VIDEO_IN_APP_STATUS_REQUEST)
			return ICommonUtils.COMPLETE_URL_FROM_MATH;
		if(requestCode == ServerOperationUtil.GET_APP_UNLOCK_STATUS_REQUEST)
			return ICommonUtils.COMPLETE_URL_FROM_MATH;
		if(requestCode == ServerOperationUtil.UPDATE_TEACHER_CREDIT_FOR_STUDENT_ANSWER)
			return ICommonUtils.COMPLETE_URL_FROM_MATH;
		if(requestCode == ServerOperationUtil.UPDATE_USER_COINS_ON_SERVER)
			return ICommonUtils.COMPLETE_URL_FROM_MATH;
		if(requestCode == ServerOperationUtil.GET_RESOURCE_CATEGORIES_REQUEST)
			return ICommonUtils.COMPLETE_URL_FROM_MATH;
		if(requestCode == ServerOperationUtil.GET_SEARCH_RESOURCES_REQUEST)
			return ICommonUtils.GOORU_SEARCH_RESOURCES_URL;	
		if(requestCode == ServerOperationUtil.GET_KHAN_VIDEO_LINK)
            return ICommonUtils.GET_KHAN_VIDEO_LINK;
		return null;
	}

	/**
	 * Create Post Request to get the app unlock status from server and save into local device
	 * @param param
	 * @return
	 */
	public static ArrayList<NameValuePair> CreatePostRequestForGetAppUnlockStatus
	(GetAppUnlockStatusParam param){
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		try{
			nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
			nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
		}
		catch (Exception e1){
			return null;
		}
		return nameValuePairs;
	}

	/**
	 * Create the post request to get the vedio resource in-app status
	 * @param param
	 * @return
	 */
	public static ArrayList<NameValuePair> CreatePostRequestForGetResourcesVedioInAppStatus
	(GetResourceVideoInAppStatusParam param){
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		try{
			nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
			nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
			nameValuePairs.add(new BasicNameValuePair("spentCoins", param.getSpentCoins() + ""));
			nameValuePairs.add(new BasicNameValuePair("appId", param.getAppId()));
		}
		catch (Exception e1){
			return null;
		}
		return nameValuePairs;
	}

	/**
	 * Create Post Request to update user coins on server
	 * @param param
	 * @return
	 */
	public static ArrayList<NameValuePair> CreatePostRequestForUpdateUserCoins
	(UpdateUserCoinsParam param){
		ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
		try{
			nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
			nameValuePairs.add(new BasicNameValuePair("userId", param.getUserId()));
			nameValuePairs.add(new BasicNameValuePair("coins", param.getCoins() + ""));
		}
		catch (Exception e1){
			return null;
		}
		return nameValuePairs;
	}
	
	/**
     * This method create post request for CreatePostRequestForGetResourcesCategories
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> CreatePostRequestForGetResourcesCategories
    (GetResourceCategoriesParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("action", param.getAction()));
            nameValuePairs.add(new BasicNameValuePair("date", param.getDate()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }   
    
    /**
     * This method create post request for GetHomeworksForStudentWithCustom
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> CreatePostRequestForGetResources(ResourceParam param){

        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("grade", param.getGrade()));
            nameValuePairs.add(new BasicNameValuePair("pageNumber", param.getPageNumber()));
            nameValuePairs.add(new BasicNameValuePair("pageSize", param.getPageSize()));
            nameValuePairs.add(new BasicNameValuePair("query", param.getQuery()));
            nameValuePairs.add(new BasicNameValuePair("resourceFormat", param.getResourceFormat()));
            nameValuePairs.add(new BasicNameValuePair("subjectName", param.getSubjectName()));
            nameValuePairs.add(new BasicNameValuePair("language", MathFriendzyHelper.
                    getResourceSelectedLanguage(MyApplication.getAppContext()) + ""));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }
    
    /**
     * This method create post request for GetHomeworksForStudentWithCustom
     * @param param
     * @return
     */
    public static ArrayList<NameValuePair> createPostRequestForGetKhanVideoLink
    (GetKhanVideoLinkParam param){
        ArrayList<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
        try{
            nameValuePairs.add(new BasicNameValuePair("link", param.getUrl()));
        }
        catch (Exception e1){
            return null;
        }
        return nameValuePairs;
    }
}
